# quadraticformula.py

import math

def quadFormula(a, b, c):
    assert (b**2) - (4*a*c) >= 0 # discriminant must be greater than 0
    return (-b + math.sqrt((b**2) - (4*a*c))) / (2*a), (-b - math.sqrt((b**2) - (4*a*c))) / (2*a)

def main():
    a = int(input("Enter value for a: "))
    b = int(input("Enter value for b: "))
    c = int(input("Enter value for c: "))
    print(quadFormula(a,b,c))

main()

