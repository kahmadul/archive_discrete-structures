# countpowerset.py


def countPowerSet(p):
    assert type(p) is set # must enter a set by using { }
    numOfElements = len(p) # len returns the length of the distinct elements from
                           #  a set (with {}'s)
                           #   in other words, the set's cardinality
                           # However, it does NOT eliminate duplicate elements from
                           #   a list (with [ ]'s)
                           #   in other words, just returns the number of elements
                           #     in a list
    return 2 ** numOfElements

def main():
    p = eval(input("Enter the set you wish examine (you must use { and } to delimit the set): "))
    print(countPowerSet(p))
    
main()
