#euclidianalgorithm.py

def GCD(a,b):
    assert a >= b # a must be the larger number
    while (b != 0):
        remainder = a % b
        a, b = b, remainder
    return a

def main():
    a = int(input("Enter first integer: "))
    b = int(input("Enter second integer: "))
    print(GCD(a,b))

main()
