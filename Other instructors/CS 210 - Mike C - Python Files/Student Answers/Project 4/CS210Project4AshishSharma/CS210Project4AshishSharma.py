#CS210Project4AshishSharma.py

print("To be a able to purchase beer at a store you must be atleast 21 years old and have your ID with you.")

Age = input("Are you atleast 21 years of age? (y/n): ")
ID = input("Do you have your ID with you? (y/n): ")

#result of AND
if Age == 'y' and ID == 'y':
    print("You may buy a beer at the store.")
    
#result of OR
if Age == 'n' or ID == 'n':
    print("You may not buy a beer at this time.")

#result of NOT
if not Age == 'y':
    print("You are not old enough to buy beer.")
