#decimaltohex.py

def decToHex(i):
    quotient = i
    answer = ""
    while quotient > 0:
        quotient, remainder = divmod(quotient,16)
        if remainder == 10:
            answer += 'A'
        elif remainder == 11:
            answer += 'B'
        elif remainder == 12:
            answer += 'C'
        elif remainder == 13:
            answer += 'D'
        elif remainder == 14:
            answer += 'E'
        elif remainder == 15:
            answer += 'F'
        else:
            answer += str(remainder)
    return answer[::-1]  # reverses the string variable answer

def simple(i):
    return hex(i) 

def main():
    i = int(input("Enter an integer greater than 0: "))
    print(decToHex(i))
    print(simple(i))

main()
        
