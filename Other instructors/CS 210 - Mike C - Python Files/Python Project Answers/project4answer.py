# project4answer.py

# remember that a string is returned from an input function, not a boolean!
# need to change p and q to booleans before using and, or, or not

def main():
    p = input("If p is true, enter True. If p is false, enter False: ")
    q = input("If q is true, enter True. If q is false, enter False: ")
    if p == 'True':
        p = True
    else:
        p = False
    if q == 'True':
        q = True
    else:
        q = False
    
    andAns = p and q
    orAns = p or q
    notAns = not p
    print("p and q:", andAns) 
    print("p or q:", orAns) 
    print("not p:", notAns) 

main()
