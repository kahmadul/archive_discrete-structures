# project10answer.py

def main():
    set1 = {1,2,3,4}
    set2 = {1,2,5,6,3,4,4,5,6}
    set3 = {1,2,3,4}
    print("Intersection of set1 and set2:", set1 & set2)
    print("Union of set1 and set2:", set1 | set2)
    print("Difference, set2 - set1:", set2 - set1)
    print("Is set1 a subset of set3?", set1 <= set2)
    print("Is set1 a proper subset of set3?", set1 < set3)

main()
