% --- ASSIGNMENT INFO --- %
\newcommand{\laClass}       {CS 210}
\newcommand{\laTopic}       {Proof by Contradiction}
\newcommand{\laKey}         {UPE4}

\input{BASE-HEAD}

% -------------------------------------------------------------------- %
\begin{intro}{Implications and their Negations}
  Recall that the negation of an implication is not also an implication,
  and that to ``disprove'' our implication, we need a counter-example
  where the \textbf{hypothesis is true and the conclusion is false.}

  \begin{center}
  \begin{tabular}{c c c}
    Implication & Negation \\

    \begin{tabular}{ | c | c || c | } \hline
        $p$     & $q$     & $p \to q$ \\ \hline
        \true   & \true   & \true       \\ \hline
        \true   & \false  & \false      \\ \hline
        \false  & \true   & \true       \\ \hline
        \false  & \false  & \true       \\ \hline
    \end{tabular}
    &
    \begin{tabular}{ | c | c || c | } \hline
        $p$     & $q$     & $p \land \neg q$  \\ \hline
        \true   & \true   & \false            \\ \hline
        \true   & \false  & \true             \\ \hline
        \false  & \true   & \false            \\ \hline
        \false  & \false  & \false            \\ \hline
    \end{tabular}
  \end{tabular}
  \end{center}
\end{intro}
% -------------------------------------------------------------------- %
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
  Given the implications, write the negation both symbolically and in English:
  
  \begin{enumerate}
    \item[(a)]    $c$: I have a cat, $h$ I am happy, $c \to h$: If I have a cat then I am happy. \vspace{2cm}
    \item[(b)]    $w$: You work hard, $r$ You get a raise, $w \to r$: If you work hard then you get a raise. \vspace{2cm}
  \end{enumerate}
\end{questionNOGRADE}
% -------------------------------------------------------------------- %

% -------------------------------------------------------------------- %
\begin{intro}{Proof by Contradiction example} \small
Example: ``For any integer $n$, if $n^2$ is even, then $n$ is even.'' ~\\
Let's look at this as an \textbf{implication}: 
``if $n^2$ is even, then $n$ is even.''
and to get a \textbf{counter-example}, the \textbf{negation} would have to be true: ~\\
``$n^2$ is even, and $n$ is \textit{not} even.''

~\\ So for our theoretical counter-example, we will assume we cound some
case where $n^2$ is even and $n$ is not even (or, odd). In this case,
we will need \textbf{two aliases}:

\begin{center}
    $n^2$ is even:  $n^2 = 2k$
    \tab
    $n$ is not even: $n = 2j+1$
\end{center}

\begin{center}
    \color{red}
    A common mistake is to re-use the same alias variable in different contexts
    (e.g., using $k$ in both cases) - never re-use the same alias variable!
    \color{black}
\end{center}

Next, we know that if we take $n \cdot n$, we get $n^2$, so we can turn
our equation with $k$ and $j$ into this:
$ n \cdot n = n^2 $    ... and then ...     
$ (2j+1)(2j+1) = 2k $

Then we simplify as much as we can:

\begin{enumerate}
    \item   $(2j+1)(2j+1) = 2k$
    \item   $4j^2 + 4j + 1 = 2k$
    \item   $2(2j^2 + 2j) + 1 = 2k$ \tab \xmark This is a contradiction!
\end{enumerate}

At this step, we've came out with the definition of an odd number equalling the definition of an even number,
which is a contradiction as it is.      
We could continue simplifying it out further, however:

\begin{enumerate}
    \item[4.]   $4j^2 + 4j - 2k = 1$            \tab[1.1cm] Moving the constant to one side
    \item[5.]   $2(2j^2 + 2j - k) = 1$          \tab Factoring out the 2
    \item[6.]   $2j^2 + 2j - k = \frac{1}{2}$   \tab[1.4cm] Dividing both sides by 2
    \item[7.]   \xmark Broken the \textbf{closure property of integers!}
\end{enumerate}

At this point, we have math on the LHS (left-hand side) between integers (multiplication, addition, subtraction)
which, given the closure property of integers, should always result in another integer.
On the RHS (right-hand side), we have a fraction. At this point, the proposition
is broken because we \textbf{cannot find a counter-example} that deals with integers.

\end{intro}
% -------------------------------------------------------------------- %
\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
  Prove with Proof by Contradiction:
  ``If $n^{2}$ is odd then $n$ is odd.''\footnote{Question from Discrete Mathematics, Ensley and Crawley, pg 147}
  
  \begin{itemize}
    \item Write out counter example ($p \land \neg q$): \vspace{1.5cm}
    \item Create aliases for $n^{2}$ and $n$: \vspace{1.5cm}
    \item Rewrite $n \cdot n = n^{2}$ using the aliases: \vspace{1.5cm}
    \item Simplify until contradiction:
  \end{itemize}
\end{questionNOGRADE}
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
  Prove with Proof by Contradiction:
  ``If $mn$ is odd, then $m$ is odd or $n$ is odd''\footnote{Question from http://www2.edc.org/makingmath/mathtools/contradiction/contradiction.asp}
  
  \begin{itemize}
    \item Write out counter example ($p \land \neg q$): \vspace{1.5cm}
    \item Create aliases for $mn$, $m$, and $n$: \vspace{1.5cm}
    \item Rewrite $mn = m \cdot n$ using the aliases: \vspace{1.5cm}
    \item Simplify until contradiction:
  \end{itemize}
\end{questionNOGRADE}
% -------------------------------------------------------------------- %


% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
\newpage
\section*{Solutions}
\solution{

\begin{enumerate}
  \item   
    \begin{enumerate}
      \item[(a)]    $c \land \neg h$: I have a cat and I am not happy.
      \item[(b)]    $w \land \neg r$: You work hard and you do not get a raise.
    \end{enumerate}
    
  \item   
    \begin{itemize}
      \item Counterexample:   $n^{2}$ is odd and $n$ is even (not odd).
      \item Aliases:          $n^{2}$ is odd, $n^{2} = 2k+1$ ~\\
                              $n$ is even, $n = 2j$
      \item Rewrite:          $n \cdot n = n^{2}$ - to - $(2j)(2j) = (2k+1)$
      \item Simplify:         $(2j)(2j) = (2k+1)$ ~\\
                              $... 2(2j^{2}) = 2k+1$ -- even definition = odd definition - this is a contradiction.
    \end{itemize}
    
  \item   
    \begin{itemize}
      \item Counterexample:   $mn$ is odd and $m$ is even and $n$ is even.
      \item Aliases:          $mn$ is odd: $mn = 2k+1$ ~\\
                              $m$ is even: $m = 2j$ ~\\
                              $n$ is even: $n = 2i$
      \item Rewrite:          $mn = m \cdot n$
      \item Simplify:         $2k+1 = (2j)(2i)$ ~\\
                              $...2k+1 = 2(2ji)$ -- odd definition = even definition - this is a contradiction.
    \end{itemize}
\end{enumerate}
}{}
\input{BASE-FOOT}


