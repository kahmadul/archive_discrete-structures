% --- ASSIGNMENT INFO --- %
\newcommand{\laClass}       {CS 210}
\newcommand{\laTopic}       {Mathematical Writing}
\newcommand{\laKey}         {UPE1}

\input{BASE-HEAD}

% -------------------------------------------------------------------- %
\begin{intro}{Turning statements to implications}
    This time we're exploring mathematical writing and getting introduced
    to proofs. This means that we are going to be working with
    \textbf{contrapositives} and \textbf{implications} some more
    in order to prove statements.
    To work with a statement, we turn it into an implication that
    we can work with mathematically.

    \paragraph{Example:}
    For every positive even integer $n$, $n+1$ is odd.

    Changing to an ``if, then" statement, we can form:

    If a positive integer $n$ is even, then $n+1$ is odd.
\end{intro}
% -------------------------------------------------------------------- %
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
  Rewrite the following statements as ``if, then" statements.
  They don't need to be \textit{true} statements, we will talk
  about disproving statements next.

  \begin{enumerate}
    \item[(a)] When a positive integer $n$ is odd, then $n+1$ is even. \vspace{2cm}

    \item[(b)] All prime numbers are odd. \vspace{1cm}
  \end{enumerate}
\end{questionNOGRADE}
% -------------------------------------------------------------------- %
\begin{intro}{Counter-examples}
    A \textbf{counter-example} is a way to disprove a proposition.
    For implications, if we can keep the \textbf{hypothesis} true,
    and find a scenario where the \textbf{conclusion} ends up being false,
    then we can disprove a statement.

    \paragraph{Example:}
    If $n$ is a prime number, then $n$ is odd.

    Can we find any examples of prime numbers that aren't odd?
    Yes - 2 is a prime number. So this statement is false.
\end{intro}
% -------------------------------------------------------------------- %
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
  Disprove the following statements by coming up with a counter-example:
  \begin{enumerate}
    \item[a.] For every even integer $n$, $n+1$ is also even. \vspace{3cm}
    \item[b.] If $n$ is a non-negative integer, then $n! > n$, where $n!$ is $n$-factorial.\footnote{$n! = n \cdot (n-1) \cdot (n-2) \cdot ... \cdot 3 \cdot 2 \cdot 1$} 
              \vspace{3cm}
    \item[c.] For every integer $n \geq 1$, if $n$ is an odd prime, then $n^{2} + 4$ is also prime. \vspace{3cm}
  \end{enumerate}
\end{questionNOGRADE}
% -------------------------------------------------------------------- %
\begin{intro}{Even and Odd}
    We will be working with the concept of even and
    odd numbers a lot, and working out proofs relating to these concepts.
    But, how do you actually specify that some number is even or odd symbolically?

    \begin{center}
        ``An integer is even if it is evenly divisible by two\\ and odd if it is not even."

        [...]

        ``A formal definition of an even number is that it is an integer of the form $n = 2k$,
        where $k$ is an integer;
        it can then be shown that an odd number is an integer of the form $n = 2k + 1$."
        \footnote{From https://en.wikipedia.org/wiki/Parity\_(mathematics)}
    \end{center}
\end{intro}
% -------------------------------------------------------------------- %
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
  Identify the following numbers as either even or odd, by writing it in either form:

  \begin{itemize}
    \item   \textbf{Even}: 2 times some other integer.
    \item   \textbf{Odd}:  2 times some other integer plus 1.
  \end{itemize}
  \paragraph{Example:}    $21 = 2 \cdot 10 + 1$

  \LARGE
  \begin{enumerate}
    \item[(a)] 7
    \item[(b)] 9
    \item[(c)] 15
    \item[(d)] 8
    \item[(e)] 16
    \item[(f)] 20
  \end{enumerate}
  \normalsize
\end{questionNOGRADE}
% -------------------------------------------------------------------- %
\begin{intro}{Divisible by...}
  We looked at the definitions for an even and odd number.
  Here's one more - divisibility!

  ~\\
  An integer $n$ is divisible by $4$ if it is the result of
  4 times some other integer. Symbolically, $n = 4k$.

  ~\\
  We can use this definition for divisible by any number, as we
  need.
\end{intro}
% -------------------------------------------------------------------- %
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
  Using the definitions of even, odd, and divisible by \textit{some integer},
  prove that the following statements are true...

  \paragraph{Example:} ``12 is even" - rewrite as 2(6).

  \begin{enumerate}
      \item[(a)] 100 is even.                              \vspace{1cm}
      \item[(b)] 13 is odd.                                \vspace{1cm}
      \item[(c)] -13 is odd.                               \vspace{1cm}
      \item[(d)] 20 is divisible by 5.                     \vspace{1cm}
      \item[(e)] 20 is divisible by 4.                     \vspace{1cm}
      \item[(f)] $6n$ is even.                             \vspace{1cm}
      \item[(g)] $8n^{2} + 8n + 4$ is divisible by 4.      \vspace{1cm}
  \end{enumerate}
\end{questionNOGRADE}
% -------------------------------------------------------------------- %
\begin{intro}{The Closure Property of Integers}
  The set of all integers is written as $\mathbb{Z}$.

  \begin{center}
      \textit{``A set has closure under an operation if
      performance of that operation on members of the set
      always produces a member of the same set; in this
      case we also say that the set is closed under the operation."}
      \footnote{From https://en.wikipedia.org/wiki/Closure\_(mathematics)}
  \end{center}

  \begin{itemize}
      \item   If you add two integers, the result is also an integer
      \item   If you subtract two integers, the result is also an integer
      \item   If you multiply two integers, the result is also an integer
      \item   If you divide two integers, the result \textbf{may not be an integer}
  \end{itemize}

  We can use these properties in our proofs, by remembering that
  if two integers $k$ and $j$ are added, the result $k+j$ is also an integer.
\end{intro}
% -------------------------------------------------------------------- %
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
  Identify if the result of the following operations belong to the set of integers $\mathbb{Z}$.
  Write the result as either $\in \mathbb{Z}$ or $\not\in \mathbb{Z}$.

  \vspace{1cm}

  \large
  \begin{tabular}{l l l l}
    (a) & 2 + 8          & $\ocircle$ $\in \mathbb{Z}$ & $\ocircle$ $\not\in \mathbb{Z}$ \\ \\
    (b) & 12 - 4         & $\ocircle$ $\in \mathbb{Z}$ & $\ocircle$ $\not\in \mathbb{Z}$ \\ \\
    (c) & 5 $\cdot$ 3    & $\ocircle$ $\in \mathbb{Z}$ & $\ocircle$ $\not\in \mathbb{Z}$ \\ \\
    (d) & 6 $\div$ 3     & $\ocircle$ $\in \mathbb{Z}$ & $\ocircle$ $\not\in \mathbb{Z}$ \\ \\
    (e) & 5 $\div$ 2     & $\ocircle$ $\in \mathbb{Z}$ & $\ocircle$ $\not\in \mathbb{Z}$ \\ \\
  \end{tabular}
  \normalsize
\end{questionNOGRADE}
% -------------------------------------------------------------------- %
\newpage
\begin{intro}{Modulus}
    ``In computing, the modulo operation finds the remainder after
    division of one number by another (sometimes called modulus).

    Given two positive numbers, a (the dividend) and n (the divisor),
    a modulo n (abbreviated as a mod n) is the remainder of the Euclidean division of a by n."
    \footnote{From https://en.wikipedia.org/wiki/Modulo\_operation}

    \begin{center}
        \includegraphics[height=3cm]{images/ch2-1-division.png}

        $9$ mod $2 = 1$
    \end{center}

    If we're dividing $a$ by $b$, the result is a quotient $q$.
    If we're calculating $a$ mod $b$, the result is the remainder $r$.

    \begin{center}
        We can also write this out as: \\
        $ a = b \cdot q + r $,
        where $0 \leq r < b$, and $q$ and $r$ are the only two integers
        that will satisfy the equation.
    \end{center}
\end{intro}

\begin{intro}{Rational numbers}
    In mathematics, a rational number is any number that can be
    expressed as the quotient or fraction p/q of two integers,
    a numerator p and a non-zero denominator q.[1] Since q may be equal to 1,
    every integer is a rational number.
    \footnote{From https://en.wikipedia.org/wiki/Rational\_number}

    The set of rational numbers is written as $\mathbb{Q}$.
\end{intro}
% -------------------------------------------------------------------- %
\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
 Solve the following modulus problems.

  \paragraph{Example:} Solve 13 mod 5
  
    \begin{center}
     
      \begin{tabular}{c c c}
          \begin{tikzpicture}
              \node at (0,0) {)};
              \draw (0,0.2) -- (1,0.2);
              
              \node at (-0.5, 0) { 5 };
              \node at (0.5, 0) { 13 };
              
              \node[white] at (1, 0.5) { 3  r1 };
              
              \node[white] at (0.5, -0.5) { 6 };
              \node[white] at (0, -0.5) { - };
              \draw[white] (0, -0.8) -- (1, -0.8);
              \node[white] at (0.5, -1.0) { 1 };
          \end{tikzpicture}
          &
          
          \begin{tikzpicture}
              \node at (0,0) {)};
              \draw (0,0.2) -- (1,0.2);
              
              \node at (-0.5, 0) { 5 };
              \node at (0.5, 0) { 13 };
              
              \node at (0.6, 0.5) { 2   };
              
              \node at (0.5, -0.5) { 10 };
              \node at (0, -0.5) { - };
              \draw (0, -0.8) -- (1, -0.8);
              \node[white] at (0.5, -1.0) { 3 };
          \end{tikzpicture}
          &
          
          \begin{tikzpicture}
              \node at (0,0) {)};
              \draw (0,0.2) -- (1,0.2);
              
              \node at (-0.5, 0) { 5 };
              \node at (0.5, 0) { 13 };
              
              \node at (1, 0.5) { 2  r3 };
              
              \node at (0.5, -0.5) { 10 };
              \node at (0, -0.5) { - };
              \draw (0, -0.8) -- (1, -0.8);
              \node at (0.5, -1.0) { 3 };
          \end{tikzpicture}
      \end{tabular}
    \end{center}
    
    So 13 mod 5 = 3.
  
  \begin{enumerate}
      \item[(a)] 9 mod 7    \vspace{3cm}
      \item[(b)] 5 mod 2    \vspace{3cm}
      \item[(c)] 15 mod 3   \vspace{3cm}
      %\item[(d)] -7 mod 2   \vspace{2cm}
  \end{enumerate}
\end{questionNOGRADE}
\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
  Write out the equations in the form of $a = b \cdot q + r$, 
  where $a \div b$ results in the quotient $q$ and remainder $r$,
  and with the constraint that $0 \leq r < b$.
  
  \paragraph{Example:} Rewrite $13 \div 5 = $ 2 r3:
    \begin{enumerate}
      \item   $a = 13$, $b = 5$; \tab $13 = 5 \cdot q + r$
      \item   $q$, the whole number from the division $13 \div 5$, is 2. \tab $13 = 5 \cdot 2 + r$
      \item   The resulting value of $r$ must be 3, and $0 \leq 3 < 5$, so this is valid.
      \item   Rewritten: $13 = 5 \cdot 2 + 3$.
    \end{enumerate}
  
  \begin{enumerate}
      \item[(a)] $9 \div 7 = $ 1 r2    \vspace{2cm}
      \item[(b)] $5 \div 2 = $ 2 r1    \vspace{2cm}
      \item[(c)] $15 \div 3 = $ 5 r0   \vspace{2cm}
      \item[(d)] $-7 \div 2 = $ -4 r1   \vspace{2cm}
  \end{enumerate}
\end{questionNOGRADE}
% -------------------------------------------------------------------- %



% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
\newpage
\section*{Solutions}
\solution{

\begin{enumerate}
  \item   
    \begin{enumerate}
      \item[(a)]  When a positive integer $n$ is odd, then $n+1$ is even.~\\
                  If $n$ is odd, then $n+1$ is even.

      \item[(b)]  All prime numbers are odd.~\\
                  If $m$ is a prime number, then $m$ is odd.
    \end{enumerate}
    
  \item 
    \begin{enumerate}
      \item[(a)] For every even integer $n$, $n+1$ is also even. ~\\
                Any even number you take, such as 2, the value of $n+1$ is NOT even. 2+1 = 3.
      \item[(b)] If $n$ is a non-negative integer, then $n! > n$, where $n!$ is $n$-factorial.\footnote{$n! = n \cdot (n-1) \cdot (n-2) \cdot ... \cdot 3 \cdot 2 \cdot 1$}  ~\\
                The value of $0! = 1$, so this disproved the statement, and so does $1! = 1$, because $1 \not> 1$.
      \item[(c)] For every integer $n \geq 1$, if $n$ is an odd prime, then $n^{2} + 4$ is also prime.~\\
                11 is an odd prime, so $11^{2} + 4 = 121 + 4 = 125$, and 125 is NOT prime.
    \end{enumerate}
  
  \item~\\
    \begin{tabular}{l l l}
      (a)   & 7     & $15 = 2 \cdot 3 + 1$ \\ \hline
      (b)   & 9     & $15 = 2 \cdot 4 + 1$ \\ \hline
      (c)   & 15    & $15 = 2 \cdot 7 + 1$ \\ \hline
      (d)   & 8     & $8 = 2 \cdot 4$      \\ \hline
      (e)   & 16    & $16 = 2 \cdot 8$     \\ \hline
      (f)   & 20    & $20 = 2 \cdot 10$    \\ \hline
    \end{tabular}
  
  \item ~\\
    \begin{tabular}{l l l}                                                                   \\ 
      (a)    & 100 is even.                         & $100 = 2(50)$                          \\ \hline
      (b)    & 13 is odd.                           & $13 = 2(6)+1$                          \\ \hline
      (c)    & -13 is odd.                          & $-13 = 2(-7) + 1$                      \\ \hline
      (d)    & 20 is divisible by 5.                & $20 = 5(4)$                            \\ \hline
      (e)    & 20 is divisible by 4.                & $20 = 4(5)$                            \\ \hline
      (f)    & $6n$ is even.                        & $6n = 2(3n)$                           \\ \hline
      (g)    & $8n^{2} + 8n + 4$ is divisible by 4. & $8n^{2} + 8n + 4 = 4(2n^{2} + 2n + 1)$ \\ \hline
    \end{tabular}
    
  \newpage
  \item ~\\
    \begin{tabular}{l l l l}
      (a) & 2 + 8  $ = 10  $        & \radio\    $\in \mathbb{Z}$ & $\ocircle$ $\not\in \mathbb{Z}$ \\
      (b) & 12 - 4 $ = 8   $        & \radio\    $\in \mathbb{Z}$ & $\ocircle$ $\not\in \mathbb{Z}$ \\
      (c) & 5 $\cdot$ 3  $ = 15  $  & \radio\    $\in \mathbb{Z}$ & $\ocircle$ $\not\in \mathbb{Z}$ \\
      (d) & 6 $\div$ 3  $ = 2   $   & \radio\    $\in \mathbb{Z}$ & $\ocircle$ $\not\in \mathbb{Z}$ \\
      (e) & 5 $\div$ 2  $ = 2.5 $   & $\ocircle$ $\in \mathbb{Z}$ & \radio\    $\not\in \mathbb{Z}$ \\
    \end{tabular}
    
  \item
    \begin{enumerate}
        \item[(a)] 9 mod 7 = 2   ~\\ 
          \begin{tikzpicture}
              \node at (0,0) {)};
              \draw (0,0.2) -- (1,0.2);
              
              \node at (-0.5, 0) { 7 };
              \node at (0.5, 0) { 9 };
              
              \node at (1, 0.5) { 1  r2 };
              
              \node at (0.5, -0.5) { 7 };
              \node at (0, -0.5) { - };
              \draw (0, -0.8) -- (1, -0.8);
              \node at (0.5, -1.0) { 2 };
          \end{tikzpicture}
          
        \item[(b)] 5 mod 2 = 1    ~\\ 
          \begin{tikzpicture}
              \node at (0,0) {)};
              \draw (0,0.2) -- (1,0.2);
              
              \node at (-0.5, 0) { 2 };
              \node at (0.5, 0) { 5 };
              
              \node at (1, 0.5) { 2  r1 };
              
              \node at (0.5, -0.5) { 4 };
              \node at (0, -0.5) { - };
              \draw (0, -0.8) -- (1, -0.8);
              \node at (0.5, -1.0) { 1 };
          \end{tikzpicture}
          
        \item[(c)] 15 mod 3 = 0  ~\\ 
          \begin{tikzpicture}
              \node at (0,0) {)};
              \draw (0,0.2) -- (1,0.2);
              
              \node at (-0.5, 0) { 3 };
              \node at (0.5, 0) { 15 };
              
              \node at (1, 0.5) { 5  r0 };
              
              \node at (0.5, -0.5) { 15 };
              \node at (0, -0.5) { - };
              \draw (0, -0.8) -- (1, -0.8);
              \node at (0.5, -1.0) { 0 };
          \end{tikzpicture}
    \end{enumerate}
    
  \item 
    \begin{enumerate}
        \item[(a)] $9 \div 7 = $ 1 r2;    \tab $9 = 7 \cdot 1 + 2$
        \item[(b)] $5 \div 2 = $ 2 r1;    \tab $5 = 2 \cdot 2 + 1$
        \item[(c)] $15 \div 3 = $ 5 r0;   \tab $15 = 3 \cdot 5 + 0$
        \item[(d)] $-7 \div 2 = $ -4 r1;  \tab $-7 = 2 \cdot (-4) + 1$
    \end{enumerate}
\end{enumerate}
}{}
\input{BASE-FOOT}


