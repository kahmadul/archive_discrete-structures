% --- ASSIGNMENT INFO --- %
\newcommand{\laClass}       {CS 210}
\newcommand{\laTopic}       {Laws for Propositional Logic}
\newcommand{\laKey}         {U1E4}

\input{BASE-HEAD}


% -------------------------------------------------------------------- %

\begin{intro}{Law of Commutation}
    The Commutative Property shows that you can rearrange the
    propositional variables in a statement like $p \lor q$
    to $q \lor p$ and they will be logically equivalent.
    This is similar to the commutative property in algebra, where something
    like $a + b = b + a$.
    \begin{center}
      \begin{tabular}{ c c c  }
        $p \lor q$            & $\equiv$    & $q \lor p$
        \\
        $p \land q$           & $\equiv$    &$q \land p$
      \end{tabular}
    \end{center}
\end{intro}

\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
  Finish the truth table to show that the following two propositional statements are equivalent:

  \begin{center}
    \colorbox{yellow!25}{$p \land \neg q$} $\equiv$ \colorbox{green!25}{$\neg q \land p$}
  \end{center}

  \begin{center} \Huge
    \begin{tabular}{ | c | c || p{3cm} || p{3cm} || p{3cm} | } \hline
      $p$       & $q$       & $\neg q$      & $p \land \neg q$        & $\neg q \land p$      \\ \hline
      \true     & \true     &               & \cellcolor{yellow!25}   & \cellcolor{green!25}  \\ \hline
      \true     & \false    &               & \cellcolor{yellow!25}   & \cellcolor{green!25}  \\ \hline
      \false    & \true     &               & \cellcolor{yellow!25}   & \cellcolor{green!25}  \\ \hline
      \false    & \false    &               & \cellcolor{yellow!25}   & \cellcolor{green!25}  \\ \hline
    \end{tabular}
  \end{center}

\end{questionNOGRADE}

% -------------------------------------------------------------------- %
\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}

  Given the propositional variables:
  \begin{itemize}
    \item   $a$: I am angry
    \item   $h$: I am hungry
  \end{itemize}

  Translate the following statement to English:  $ a \lor h $

  \vspace{2cm}

  Translate the equivalent statement to English: $ h \lor a $

  \vspace{2cm}
\end{questionNOGRADE}

\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}

  Given the propositional variables:
  \begin{itemize}
    \item   $m$: I use a Mac computer
    \item   $i$: I use an iPhone
  \end{itemize}

  Translate the following statement to English:  $ m \land i $

  \vspace{2cm}

  Translate the equivalent statement to English: $ i \land m $

  \vspace{2cm}
\end{questionNOGRADE}

% -------------------------------------------------------------------- %
\newpage
\begin{intro}{Law of Association}
  The Associative Property shows that, when working with all $\lor$ or all $\land$ in
  a propositional statement (not $\lor$ and $\land$ mixed together),
  evaluating any two propositional variables can be done in any order.
  In algebra, this is like $(1 + 2) + 3 = 1 + (2 + 3)$ or $1 \cdot (2 \cdot 3) = (1 \cdot 2) \cdot 3)$.

  \begin{center}
    \begin{tabular}{ c c c  }
      $p \lor (q \lor r)$   & $\equiv$    & $(p \lor q) \lor r$
      \\
      $p \land (q \land r)$ & $\equiv$    & $(p \land q) \land r$
    \end{tabular}
  \end{center}
\end{intro}

\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
  Finish the truth table to show that the following two propositional statements are equivalent:

  \begin{center}
    \colorbox{yellow!25}{$p \lor (q \lor r)$} $\equiv$ \colorbox{green!25}{$(p \lor q) \lor r$}
  \end{center}

  \begin{center} \Huge
    \begin{tabular}{ | c | c | c || p{4cm} || p{4cm} | } \hline
      $p$       & $q$       & $r$       & $p \lor (q \lor r)$       & $(p \lor q) \lor r$       \\ \hline
      \true     & \true     & \true     & \cellcolor{yellow!25}     & \cellcolor{green!25}      \\ \hline
      \true     & \true     & \false    & \cellcolor{yellow!25}     & \cellcolor{green!25}      \\ \hline
      \true     & \false    & \true     & \cellcolor{yellow!25}     & \cellcolor{green!25}      \\ \hline
      \true     & \false    & \false    & \cellcolor{yellow!25}     & \cellcolor{green!25}      \\ \hline
      \false    & \true     & \true     & \cellcolor{yellow!25}     & \cellcolor{green!25}      \\ \hline
      \false    & \true     & \false    & \cellcolor{yellow!25}     & \cellcolor{green!25}      \\ \hline
      \false    & \false    & \true     & \cellcolor{yellow!25}     & \cellcolor{green!25}      \\ \hline
      \false    & \false    & \false    & \cellcolor{yellow!25}     & \cellcolor{green!25}      \\ \hline
    \end{tabular}
  \end{center}
\end{questionNOGRADE}

% -------------------------------------------------------------------- %
\newpage
\begin{intro}{Law of Distribution}
  The Distributive Property shows that when we have a statement like
  $p \lor (q \land r)$, we expand the statement by ``distributing''
  the outer variable to both of the internal variables: $(p \lor q) \land (p \land r)$.
  In algebra, we have the distributive property with multiplication, like
  $x(y+z) = xy + xz$.

  \begin{center}
    \begin{tabular}{ c c c  }
      $p \land (q \lor r)$  & $\equiv$    & $(p \land q) \lor (p \land r)$
      \\
      $p \lor (q \land r)$  & $\equiv$    & $(p \lor q) \land (p \lor r)$
    \end{tabular}
  \end{center}
\end{intro}

\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
  Finish the truth table to show that the following two propositional statements are equivalent:

  \begin{center}
    \colorbox{yellow!25}{$p \land (q \lor r)$} $\equiv$ \colorbox{green!25}{$(p \land q) \lor (p \land r)$}
  \end{center}

  \begin{center} \large
    \begin{tabular}{ | c | c | c | c || c || c | c || c | } \hline
      $p$       & $q$       & $r$       & $q \lor r$            & $p \land (q \lor r)$        & $p \land q$       & $p \land r$       & $(p \land q) \lor (p \land r)$              \\ \hline
      \true     & \true     & \true     &                       & \cellcolor{yellow!25}       &                   &                   & \cellcolor{green!25}                        \\ \hline
      \true     & \true     & \false    &                       & \cellcolor{yellow!25}       &                   &                   & \cellcolor{green!25}                        \\ \hline
      \true     & \false    & \true     &                       & \cellcolor{yellow!25}       &                   &                   & \cellcolor{green!25}                        \\ \hline
      \true     & \false    & \false    &                       & \cellcolor{yellow!25}       &                   &                   & \cellcolor{green!25}                        \\ \hline
      \false    & \true     & \true     &                       & \cellcolor{yellow!25}       &                   &                   & \cellcolor{green!25}                        \\ \hline
      \false    & \true     & \false    &                       & \cellcolor{yellow!25}       &                   &                   & \cellcolor{green!25}                        \\ \hline
      \false    & \false    & \true     &                       & \cellcolor{yellow!25}       &                   &                   & \cellcolor{green!25}                        \\ \hline
      \false    & \false    & \false    &                       & \cellcolor{yellow!25}       &                   &                   & \cellcolor{green!25}                        \\ \hline
    \end{tabular}
  \end{center}
\end{questionNOGRADE}


% -------------------------------------------------------------------- %
\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}

  Given the propositional variables:
  \begin{itemize}
    \item   $w$: I have work today
    \item   $k$: I have to pick up my kids today
    \item   $s$: I have school today
  \end{itemize}

  Translate the following statement to English:  $ w \land (k \lor s) $

  \vspace{2cm}

  Translate the equivalent statement to English: $ (w \land k) \lor (w \land s) $

  \vspace{2cm}
\end{questionNOGRADE}

\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}

  Given the propositional variables:
  \begin{itemize}
    \item   $w$: I work today
    \item   $c$: I clean the house today
    \item   $d$: I cook dinner today
  \end{itemize}

  Translate the following statement to English:  $ w \lor (c \land d) $

  \vspace{2cm}

  Translate the equivalent statement to English: $ (w \lor c) \land (w \lor d) $

  \vspace{2cm}
\end{questionNOGRADE}



% -------------------------------------------------------------------- %
\newpage
\begin{intro}{DeMorgan's Law}
    DeMorgan's laws describe the result of the negation of the statement $(p \lor q)$,
    which comes out to $\neg p \land \neg q$,
    and the negation of the statement $(p \land q)$, which comes out to $\neg p \lor \neg q$.

  \begin{center}
    \begin{tabular}{ c c c  }
      $\neg(p \land q)$     & $\equiv$    & $\neg p \lor \neg q$
      \\
      $\neg(p \lor q)$      & $\equiv$    & $\neg p \land \neg q$
    \end{tabular}
  \end{center}
\end{intro}

\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
  Finish the truth table to show that the following two propositional statements are equivalent:

  \begin{center}
    \colorbox{yellow!25}{$\neg(p \lor q)$} $\equiv$ \colorbox{green!25}{$\neg p \land \neg q$}
  \end{center}

  \begin{center} \Huge
    \begin{tabular}{ | c | c | c || c || c | c || c | } \hline
      $p$       & $q$       & $p \lor q$    & $\neg(p \lor q)$          & $\neg p$    & $\neg q$      & $\neg p \land \neg q$       \\ \hline
      \true     & \true     &               & \cellcolor{yellow!25}     &             &               & \cellcolor{green!25}        \\ \hline
      \true     & \false    &               & \cellcolor{yellow!25}     &             &               & \cellcolor{green!25}        \\ \hline
      \false    & \true     &               & \cellcolor{yellow!25}     &             &               & \cellcolor{green!25}        \\ \hline
      \false    & \false    &               & \cellcolor{yellow!25}     &             &               & \cellcolor{green!25}        \\ \hline
    \end{tabular}
  \end{center}
\end{questionNOGRADE}


% -------------------------------------------------------------------- %
\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}

  Given the propositional variables:
  \begin{itemize}
    \item   $c$: it is cold outside
    \item   $r$: it is raining outside
  \end{itemize}

  Translate the following statement to English: $ \neg ( c \lor r ) $

  \vspace{2cm}

  Translate the equivalent statement to English: $ \neg c \land \neg r $

  \vspace{2cm}
\end{questionNOGRADE}

\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}

  Given the propositional variables:
  \begin{itemize}
    \item   $c$: I have a cat
    \item   $d$: I have a dog
  \end{itemize}

  Translate the following statement to English: $ \neg ( c \land r ) $

  \vspace{2cm}

  Translate the equivalent statement to English: $ \neg c \lor \neg r $

  \vspace{2cm}
\end{questionNOGRADE}


% -------------------------------------------------------------------- %
%\newpage
%\begin{intro}{Laws for Propositional Logic}
  %\begin{center}
    %\begin{tabular}{| l | c c c | } \hline
      %Commutation         & $p \lor q$            & $\equiv$    & $q \lor p$
      %\\                  & $p \land q$           & $\equiv$    &$q \land p$         
      %\\ \hline
      %Association         & $p \lor (q \lor r)$   & $\equiv$    & $(p \lor q) \lor r$
      %\\                  & $p \land (q \land r)$ & $\equiv$    & $(p \land q) \land r$
      %\\ \hline
      %Distribution        & $p \land (q \lor r)$  & $\equiv$    & $(p \land q) \lor (p \land r)$
      %\\                  & $p \lor (q \land r)$  & $\equiv$    & $(p \lor q) \land (p \lor r)$
      %\\ \hline
      %DeMorgan's Theorem  & $\neg(p \land q)$     & $\equiv$    & $\neg p \lor \neg q$
      %\\                  & $\neg(p \lor q)$      & $\equiv$    & $\neg p \land \neg q$
      %\\ \hline
      %Double negative     & $p$                   & $\equiv$    & $\neg(\neg p)$
      %\\ \hline
      %Tautology           & $p \lor \neg p$       & $\equiv$    & true
      %\\ \hline
      %Contradiction       & $p \land \neg p$      & $\equiv$    & false
      %\\ \hline
    %\end{tabular}
  %\end{center}
%\end{intro}

%\stepcounter{question}
%\begin{questionNOGRADE}{\thequestion}
  %Simplify the statement, also mention which law you're using.
%\end{questionNOGRADE}

% -------------------------------------------------------------------- %

% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
\newpage
\section*{Solutions}
\solution{

\begin{enumerate}
  \item Law of Commutation ~\\
    \begin{tabular}{ | c | c || p{3cm} || p{3cm} || p{3cm} | } \hline
      $p$       & $q$       & $\neg q$      & $p \land \neg q$      & $\neg q \land p$  \\ \hline
      \true     & \true     & \false        & \false                & \false            \\ \hline
      \true     & \false    & \true         & \true                 & \true             \\ \hline
      \false    & \true     & \false        & \false                & \false            \\ \hline
      \false    & \false    & \true         & \false                & \false            \\ \hline
    \end{tabular}
    
  \item   $ a \lor h $ : I am angry or I am hungry.
      ~\\ $ h \lor a $ : I am hungry or I am angry.

  \item   $ m \land i $ : I use a Mac computer and I use an iPhone.
      ~\\ $ i \land m $ : I use an iPhone and I use a Mac computer.

  \item Law of Association ~\\
    \begin{tabular}{ | c | c | c || p{3cm} || p{3cm} | } \hline
      $p$       & $q$       & $r$       & $p \lor (q \lor r)$   & $(p \lor q) \lor r$   \\ \hline
      \true     & \true     & \true     & \true                 & \true                 \\ \hline
      \true     & \true     & \false    & \true                 & \true                 \\ \hline
      \true     & \false    & \true     & \true                 & \true                 \\ \hline
      \true     & \false    & \false    & \true                 & \true                 \\ \hline
      \false    & \true     & \true     & \true                 & \true                 \\ \hline
      \false    & \true     & \false    & \true                 & \true                 \\ \hline
      \false    & \false    & \true     & \true                 & \true                 \\ \hline
      \false    & \false    & \false    & \false                & \false                \\ \hline
    \end{tabular}

  \item Law of Distribution ~\\
    \begin{tabular}{ | c | c | c | c || c || c | c || c | } \hline
      $p$       & $q$       & $r$       & $q \lor r$            & $p \land (q \lor r)$        & $p \land q$       & $p \land r$       & $(p \land q) \lor (p \land r)$              \\ \hline
      \true     & \true     & \true     & \true                 & \true                       & \true             & \true             & \true                                       \\ \hline
      \true     & \true     & \false    & \true                 & \true                       & \true             & \false            & \true                                       \\ \hline
      \true     & \false    & \true     & \true                 & \true                       & \false            & \true             & \true                                       \\ \hline
      \true     & \false    & \false    & \false                & \false                      & \false            & \false            & \false                                      \\ \hline
      \false    & \true     & \true     & \true                 & \false                      & \false            & \false            & \false                                      \\ \hline
      \false    & \true     & \false    & \true                 & \false                      & \false            & \false            & \false                                      \\ \hline
      \false    & \false    & \true     & \true                 & \false                      & \false            & \false            & \false                                      \\ \hline
      \false    & \false    & \false    & \false                & \false                      & \false            & \false            & \false                                      \\ \hline
    \end{tabular}

  \item     $ w \land (k \lor s) $ : I have work today and I either need to pick up my kids today or go to school today.
        ~\\ $ (w \land k) \lor (w \land s) $ : I have work and I have to pick up my kids today OR I have work and I have to go to school today.

  \newpage
  \item     $ w \lor (c \land d) $ : I work today OR I clean the house today AND cook dinner today.
    ~\\     $ (w \lor c) \land (w \lor d) $ : I work or clean the house today AND I work or cook dinner today.
    ~\\     I know this sounds really weird to phrase it that way, but basically if work is false, then that means they have to clean for
            the first part and cook for the second part. Remember that our logical ``OR'' is not EXCLUSIVE OR. The original statement,
            ``I work today OR I clean the house today AND cook dinner today.'', they could still be doing all 3 and the statement would be true.

  \item DeMorgan's Law ~\\
    \begin{tabular}{ | c | c | c || c || c | c || c | } \hline
      $p$       & $q$       & $p \lor q$    & $\neg(p \lor q)$          & $\neg p$    & $\neg q$      & $\neg p \land \neg q$       \\ \hline
      \true     & \true     & \true         & \false                    & \false      & \false        & \false                      \\ \hline
      \true     & \false    & \true         & \false                    & \false      & \true         & \false                      \\ \hline
      \false    & \true     & \true         & \false                    & \true       & \false        & \false                      \\ \hline
      \false    & \false    & \false        & \true                     & \true       & \true         & \true                       \\ \hline
    \end{tabular}

  \item     $ \neg ( c \lor r ) $ : It is not true that ``it is cold or it is raining''.
        ~\\ $ \neg c \land \neg r $ : It is not cold and it is not raining.
        ~\\ (It is neither cold nor raining, so therefore saying ``it is cold or raining'' is false.)

  \item     $ \neg ( c \land r ) $ : It is not true that ``I have a cat and I have a dog''.
        ~\\ $ \neg c \lor \neg r $ : I do not have a cat or I do not have a dog.
        ~\\ (Thus, having both is not true; you must have one or the other.)

\end{enumerate}
}{}
\input{BASE-FOOT}


