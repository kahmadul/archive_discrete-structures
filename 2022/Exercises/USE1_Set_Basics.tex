% --- ASSIGNMENT INFO --- %
\newcommand{\laClass}       {CS 210}
\newcommand{\laTopic}       {Set Basics}
\newcommand{\laKey}         {USE1}

\input{BASE-HEAD}

% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
\begin{intro}{Set notation and basics}
  A set is a structure that contains information. Often,
  our sets will contain numbers, but they could also contain
  a list of colors, students, and so on. The items that are
  contained in a set are known as the \textbf{elements} of the set.

  \paragraph{Notation:} Sets are usually given single, capital letters
  as the names. Then, to specify the elements within the set,
  you write it between opening and closing curly-braces \{ \},
  with each element separated by a comma.

  \begin{center}
      $A = \{ 1, 2, 3, 4 \}$
  \end{center}
\end{intro}
% -------------------------------------------------------------------- %
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
  Define the following sets...

  \begin{enumerate}
      \item[(a)]  The set $R$ of your favorite colors.
                  \vspace{1cm}
      \item[(b)]  The set $T$ of weather temperature highs for the week
                  \vspace{1cm}
      \item[(c)]  A set $O$ of all odd integers
                  \vspace{1cm}
      \item[(d)]  An empty set
                  \vspace{1cm}
  \end{enumerate}
\end{questionNOGRADE}


% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
\newpage
\begin{intro}{More info about sets}
    \textbf{Order doesn't matter:} Two sets with the same elements
    but a different order are treated as the same.

    \paragraph{Duplicates don't matter:} If an element shows up in a
    set more than once, it is still considered the same set as if
    it had just had one of that element.
\end{intro}
% -------------------------------------------------------------------- %
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
Given the sets:

\begin{center}
    $A = \{ 1, 2, 3 \}$ \tab $B = \{ 2, 2, 3 \}$ \tab
    $C = \{ 5, 6, 7 \}$ \tab $D = \{ 3, 2, 1\} $
\end{center}

For the following, fill in whether the two sets are
\textbf{equal} (=) or \textbf{not} ($\neq$). ~\\

\begin{center}
    $A$ \fitb $B$ \tab[2cm] $A$ \fitb $C$ \tab[2cm] $A$ \fitb $D$
\end{center}
\end{questionNOGRADE}

\hrulefill
    
\begin{intro}{Common sets of numbers}
    Some common sets we will be working with are...
    ~\\
    $\mathbb{Z}$, \footnotesize the set of integers; whole numbers - positive, negative, and 0. \normalsize
    ~\\
    $\mathbb{N}$, \footnotesize the set of natural numbers; counting numbers - 0 and positive integers. \normalsize
    ~\\
    $\mathbb{Q}$, \footnotesize the set of rational numbers; any number that can be written as a fraction/ratio. \normalsize
    ~\\
    $\mathbb{R}$, \footnotesize the set of real numbers; all of the above, plus numbers with unending strings of digits after the decimal point. \normalsize
\end{intro}
\normalsize

\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
    For the following numbers, check $\checkmark$ which set(s) they belong to.
    \large

    \begin{center}
        \begin{tabular}{| c | p{1cm} | p{1cm} | p{1cm} | p{1cm} |} \hline
            & $\mathbb{N}$ & $\mathbb{Z}$ & $\mathbb{Q}$ & $\mathbb{R}$ \\ \hline
            10      & & & &    \\ \hline
            -5      & & & &    \\ \hline
            12/6    & & & &    \\ \hline
            $\pi$   & & & &    \\ \hline
            2.40    & & & &    \\ \hline
        \end{tabular}
    \end{center}
\end{questionNOGRADE}

% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
\newpage
\begin{intro}{Subsets}
  \begin{tabular}{p{3.5cm} p{8.5cm}}
    $x$ exists in $A$       & The notation $x \in A$ means ``$x$ is an element of the set A''. 
    \\ \\
    $A$ is a subset of $B$  & Written as $A \subseteq B$, if every element of $A$ is also an element of $B$, then $A$ is a subset of $B$.
                                                            
                              Written more formally, for every $x$, if $x \in A$, then $x \in B$.
                              
    \\
    $A$ is equal to $B$     & $A$ is equal to $B$ (written $A = B$) if both sets have the same elements.
    
                              Written formally, if $A \subseteq B$ and $B \subseteq A$, then $A = B$.
    
    \\
    An Empty Set            & A set that contains no elements is called an empty set and is written as $\{\}$ or $\emptyset$.
    \\ \\
    The Universal Set       & For any given discussion, all sets will be subsets of a larger set called the \textit{universal set} (or universe), commonly written as $U$.
  \end{tabular}
\end{intro}
% -------------------------------------------------------------------- %
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
    For each scenario, check any items that are \textbf{true}. 
    \begin{enumerate}
        \item[(a)]   $A = \{ 1, 2, 3 \}$ and $B = \{2, 3\}$ ~\\
                    \Square\ $A \subseteq B$        \tab
                    \Square\ $A \not\subseteq B$    \tab
                    \Square\ $B \subseteq A$        \tab
                    \Square\ $B \not\subseteq A$    \tab
                    \Square\ $A = B$         
        \vspace{1cm}
        \item[(b)]   $C = \{ red, green, blue \}$ and $D = \{ red, yellow, blue \}$ ~\\
                    \Square\ $C \subseteq D$        \tab[0.7cm]
                    \Square\ $C \not\subseteq D$    \tab[0.7cm]
                    \Square\ $D \subseteq C$        \tab[0.7cm]
                    \Square\ $D \not\subseteq C$    \tab[0.7cm]
                    \Square\ $C = D$         
        \vspace{1cm}
        \item[(c)]   $E = \{ 1, 1, 2, 3, 4 \}$ and $F = \{ 4, 3, 2, 2, 1 \}$ ~\\
                    \Square\ $E \subseteq F$        \tab[0.7cm]
                    \Square\ $E \not\subseteq F$    \tab[0.7cm]
                    \Square\ $F \subseteq E$        \tab[0.7cm]
                    \Square\ $F \not\subseteq E$    \tab[0.7cm]
                    \Square\ $E = F$ 
    \end{enumerate}
\end{questionNOGRADE}
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
\newpage
\begin{intro}{Set cardinality}
\textbf{Set cardinality} is a way of saying the ``size of the set'',
    or the ``amount of elements in the set''. It can be denoted as $|X|$ or $n(X)$,
    depending on the book.
\end{intro}

\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
    Give the set cardinality for each.
    ~\\~\\
    \begin{tabular}{l l l}
      (a) & $A = \{ 1, 2, 3 \}$ & $|A| = $ \\ \\
      (b) & $B = \{ \}$ & $|B| = $  \\ \\
      (c) & $C = \{ \{ a, b, c \}, \{ 1, 2, 3 \} \}$ & $|C| = $
    \end{tabular}
\end{questionNOGRADE}


\vspace{3cm}
(More on next page)

% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
\newpage
\begin{intro}{Intersections, unions, and differences}
  \small
  \begin{tabular}{p{4.5cm} p{8cm}}
    Intersection of $A$ and $B$   & $A \cap B$ is a set that contains elements common to both $A$ and $B$.
                                    In set-builder notation, we write
                                    
                                    $A \cap B = \{ x \in U : x \in A \land x \in B \}$
    \\ \\
    Union of $A$ and $B$          & $A \cup B$ is a set that contains elements that are in either $A$ and/or $B$.
                                    In set-builder notation, we write
                                    
                                    $A \cup B = \{ x \in U : x \in A \lor x \in B \}$
    \\ \\
    Difference of $A$ and $B$     & $A - B$ is a set that contains elements in $A$ which are NOT also in $B$.
                                    In set-builder notation, we write
                                    
                                    $A - B = \{ x \in U : x \in A \land x \not\in B \}$
    \\ \\
    Disjoint sets                 & Sets $A$ and $B$ are disjoint if $A \cap B = \emptyset$.
                                    In other words, they are disjoint if they have no elements in common.
                                    
    \\ \\
    The complement of $A$         & $A'$ is a set with elements from the universe $U$ except for elements from the set $A$.
                                    We can define it as $A' = U - A$.
  \end{tabular}
  
  \vspace{0.5cm}
  
  \hrulefill

  \vspace{0.5cm}
  
  \normalsize
  
  Venn diagrams are used to visually represent relationships between sets.
  Sets $A$ and $B$ (or more) are drawn as overlapping circles, and the
  shaded-in region represents the set based on the \textit{intersection},
  \textit{union}, \textit{complement}, or \textit{difference} operations.
  
  \def\circleA{(1.0,1.5) circle (0.75cm)}
  \def\circleB{(2.0,1.5) circle (0.75cm)}
  
  \begin{center}
    \begin{tabular}{c c c c}
      \begin{tikzpicture}
             \draw[fill=white] (0,0) -- (3,0) -- (3,3) -- (0,3) -- (0,0) node[anchor=south west] {U};
             \begin{scope}
                 \clip \circleA;
                 \fill[fill=orange] \circleB;
             \end{scope}
             \draw \circleA node[anchor=south east] {$A$};
             \draw \circleB node[anchor=south west] {$B$};
         \end{tikzpicture}
      &
      \begin{tikzpicture}
          \draw[fill=white] (0,0) -- (3,0) -- (3,3) -- (0,3) -- (0,0) node[anchor=south west] {U};
          \draw[fill=orange]  \circleA node[anchor=south east] {$A$}
                              \circleB node[anchor=south west] {$B$};
      \end{tikzpicture}
      &
      \begin{tikzpicture}
         \draw[fill=white] (0,0) -- (3,0) -- (3,3) -- (0,3) -- (0,0) node[anchor=south west] {U};
         \begin{scope}
             \clip \circleA;{}
             \draw[fill=orange, even odd rule]   \circleA node[anchor=south east] {$A$}
                                                 \circleB;
         \end{scope}
         \draw[]             \circleA node[anchor=south east] {$A$}
                             \circleB node[anchor=south west] {$B$};
      \end{tikzpicture}
      &
      \begin{tikzpicture}
          \draw[fill=orange] (0,0) -- (2,0) -- (2,3) -- (0,3) -- (0,0) node[anchor=south west] {$U$};
          \draw[fill=white] (1.0, 1.5) circle (0.75cm) node[anchor=south east] {$A$};
      \end{tikzpicture}
        
      \\
      $A \cap B$ & $A \cup B$ & $A - B$ & $A'$
    \end{tabular}
  \end{center}
\end{intro}\normalsize
% -------------------------------------------------------------------- %
\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}

    Fill in the Venn diagrams for the following operations: ~\\~\\
    \begin{tabular}{l l l}
        a.      $A \cap B$
        & b.    $A \cap C$
        & c.    $(A \cap B) \cup C$
        \\
        \venndiagram &
        \venndiagram &
        \venndiagram
        \\ \\
        d.      $B \cup C$
        & e.    $A \cap (B\cup C)$
        & f.    $B - C$
        \\
        \venndiagram &
        \venndiagram &
        \venndiagram
        \\ \\
        g.      $A'$
        & h.    $(A \cap B)'$
        & i.    $(A \cup B) - C$
        \\
        \venndiagram &
        \venndiagram &
        \venndiagram
    \end{tabular}
\end{questionNOGRADE}
% -------------------------------------------------------------------- %
\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
Given the following sets, compute the set operations in order to show that
the following statements are true.

  \begin{center}
    $U = \{ 1, 2, 3, 4, 5, 6, 7, 8 \}$  \tab[0.2cm]
    $A = \{ 1, 3, 5 \}$                 \tab[0.2cm]
    $B = \{ 1, 2, 3, 4 \}$              \tab[0.2cm]
    $C = \{ 1, 2, 5, 6, 8 \}$
  \end{center} ~\\

  \begin{enumerate}
    \item[(a)] $A \cap (B \cup C) = (A \cap B) \cup (A \cap C)$   \vspace{4cm}
    \item[(b)] $(A \cup B)' = A' \cap B'$                         \vspace{4cm}
    \item[(c)] $A \cap (A \cup B) = A$                            \vspace{4cm}
  \end{enumerate}
\end{questionNOGRADE}


% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
\newpage
\begin{intro}{Set-builder notation} \small
  It is impractical to try to list every element of a set in some cases.
  We use set-builder notation to describe most sets. There are two different
  forms we can use.
  
  ~\\
  A \textbf{Property Description} is of the form, ``The set of all $x$ in $U$,
  such that $x$ is \fitb.'' The blank is \textit{some property} of $x$, which determines
  whether an element of the universe $U$ is or is not in the set.
  
  \begin{itemize}
    \item The set of even integers: $\{ x \in \mathbb{Z} : x = 2y$ for some $y \in \mathbb{Z} \}$
    \item The set of real numbers bigger than 10: $\{ x \in \mathbb{R} : x > 10 \}$
  \end{itemize}
  
  ~\\
  A \textbf{Form Description} is of the form, ``All numbers of the form \fitb,{}
  where $x$ is in the set $D$.'' The first part will be some equation (like ``$2x$'', for even).
  
  \begin{itemize}
    \item The set of even integers: $\{ 2k : k \in \mathbb{Z} \}$
    \item The set of perfect square integers: $\{ m^{2} : m \in \mathbb{Z} \}$
  \end{itemize}
\end{intro}\normalsize
% -------------------------------------------------------------------- %
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
Write the following set description in \textbf{property description} set-builder notation
using the given steps to help figure it out.

\begin{center}
  ``The set of all odd integers''
\end{center}
~\\
\begin{tabular}{l l l}
  Step 1. & Using $x$ as the variable, what set does $x$ belong to? & $x \in \fitb$ \\ \\
  Step 2. & In English, how would you describe $x$? & $x$ is \fitb \\ \\
  Step 3. & How would you write Step 2 symbolically? & $x = \fitb$ \\ \\
  Step 4. & For the \textbf{Property Description}, \\
          & the form is $\{ set : property \}$.
\end{tabular}
~\\~\\~\\
\begin{tabular}{c c c c c c c c c}
  $\{ x \in $   & \fitb       & : & \fitb[2cm]   & for some  & \fitb           & $\in$ & \fitb       & $\}$      \\
                & Step 1 set  &   & Step 3  &           & $2^{nd}$ var    &       & Step 1 set
\end{tabular}

\end{questionNOGRADE}

% -------------------------------------------------------------------- %
\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
Write the following set description in \textbf{form description} set-builder notation
using the given steps to help figure it out.

\begin{center}
  ``The set of all integers divisible by 3''
\end{center}

~\\
\begin{tabular}{l l l}
  Step 1. & Using $x$ as the variable, what set does $x$ belong to? & $x \in \fitb$ \\ \\
  Step 2. & In English, how would you describe $x$? & $x$ is \fitb \\ \\
  Step 3. & How would you write Step 2 symbolically? & $x = \fitb$ \\ \\
  Step 4. & For the \textbf{Form Description}, \\
          & the form is $\{ form : set \}$.
\end{tabular}
~\\~\\~\\
\begin{tabular}{c c c c c c c}
  $\{$ & \fitb       & : & \fitb[2cm]      & $\in$   & \fitb       & $\}$      \\
       & Step 3 RHS  &   & Step 3 RHS var  &         & Step 1 set
\end{tabular}
~\\~\\
(RHS stands for ``right-hand side'', the right hand side of the $=$ sign.)

\end{questionNOGRADE}


% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
\newpage
\section*{Solutions}
\solution{
\begin{enumerate}
  \item   Multiple solutions
    \begin{enumerate}
      \item   $R = \{$ purple, orange, green $\}$
      \item   $T = \{ 22, 39, 35, 58, 61, 29, 30 \}$
      \item   $O = \{ 1, 3, 5, 7, 9 \}$
      \item   $E = \{ \}$ or $E = \emptyset$
    \end{enumerate}
  
  \item   $A \neq B$ \tab $A \neq C$ \tab $A = D$
  
  \item 
        \begin{tabular}{| c | c | c | c | c | p{6cm} | } \hline
                    & $\mathbb{N}$  & $\mathbb{Z}$  & $\mathbb{Q}$  & $\mathbb{R}$  &       \\ \hline
            10      & $\checkmark$  & $\checkmark$  & $\checkmark$  & $\checkmark$  & Any integer can be written as a fraction, like $\frac{10}{1}$.      \\ \hline
            -5      &               & $\checkmark$  & $\checkmark$  & $\checkmark$  &       \\ \hline
            12/6    &               & $\checkmark$  & $\checkmark$  & $\checkmark$  & $\frac{12}{6}$ reduces to 2, so it is an integer.      \\ \hline
            $\pi$   &               &               &               & $\checkmark$  & $\pi$ can't be represented by a fraction.      \\ \hline
            2.40    &               &               & $\checkmark$  &               & 2.40 is equivalent to $\frac{12}{5}$      \\ \hline
        \end{tabular}
        
  \item   
    \begin{tabular}{l l l}
        (a) & $A = \{ 1, 2, 3 \}$ and $B = \{2, 3\}$                          & $A \not\subseteq B$ , $B \subseteq A$ \\
        (b) & $C = \{ red, green, blue \}$ and $D = \{ red, yellow, blue \}$  & $C \not\subseteq D$ , $D \not\subseteq C$ \\
        (c) & $E = \{ 1, 1, 2, 3, 4 \}$ and $F = \{ 4, 3, 2, 2, 1 \}$         & $E \subseteq F$ , $F \subseteq E$ , $E = F$  
    \end{tabular}
    
  \item 
    \begin{tabular}{l l l}
      (a) & $A = \{ 1, 2, 3 \}$ & $|A| = 3$ \\
      (b) & $B = \{ \}$ & $|B| = 0$  \\
      (c) & $C = \{ \{ a, b, c \}, \{ 1, 2, 3 \} \}$ & $|C| = 2$
    \end{tabular}
    
    $C$ is a set that contains two elements, even though those elements are also sets themselves.
  
  \newpage
  \item ~\\
  
    \def\circleA{(1.5,2.5) circle (1.0cm)}
    \def\circleB{(2.5,2.5) circle (1.0cm)}
    \def\circleC{(2.0,1.5) circle (1.0cm)}
    
    \begin{tabular}{l l l}
        a.      $A \cap B$
        & b.    $A \cap C$
        & c.    $(A \cap B) \cup C$
        \\   
        \begin{tikzpicture}
            \draw (0,0) -- (4,0) -- (4,4) -- (0,4) -- (0,0) node[anchor=south west] {U};
            \begin{scope}
                 \clip \circleA;
                 \fill[fill=pink] \circleB;
            \end{scope}
            \draw            	\circleA circle (1cm) node[anchor=south east] {A};
            \draw             \circleB circle (1cm) node[anchor=south west] {B};
            \draw             \circleC circle (1cm) node[below] {C};
        \end{tikzpicture}
        &
        \begin{tikzpicture}
            \draw (0,0) -- (4,0) -- (4,4) -- (0,4) -- (0,0) node[anchor=south west] {U};
            \begin{scope}
                 \clip \circleA;
                 \fill[fill=pink] \circleC;
            \end{scope}
            \draw            	\circleA circle (1cm) node[anchor=south east] {A};
            \draw             \circleB circle (1cm) node[anchor=south west] {B};
            \draw             \circleC circle (1cm) node[below] {C};
        \end{tikzpicture}
        &
        \begin{tikzpicture}
            \draw (0,0) -- (4,0) -- (4,4) -- (0,4) -- (0,0) node[anchor=south west] {U};
            \begin{scope}
                 \clip \circleA;
                 \fill[fill=pink] \circleB;
            \end{scope}
            \draw            	\circleA circle (1cm) node[anchor=south east] {A};
            \draw             \circleB circle (1cm) node[anchor=south west] {B};
            \draw[fill=pink]  \circleC circle (1cm) node[below] {C};
            
            \draw            	\circleA circle (1cm) node[anchor=south east] {A};
            \draw             \circleB circle (1cm) node[anchor=south west] {B};
            \draw             \circleC circle (1cm) node[below] {C};
        \end{tikzpicture}
        \\ \\
        d.      $B \cup C$
        & e.    $A \cap (B\cup C)$
        & f.    $B - C$
        \\
        \begin{tikzpicture}
            \draw (0,0) -- (4,0) -- (4,4) -- (0,4) -- (0,0) node[anchor=south west] {U};
            \draw            	\circleA circle (1cm) node[anchor=south east] {A};
            \draw[fill=pink]  \circleB circle (1cm) node[anchor=south west] {B};
            \draw[fill=pink]  \circleC circle (1cm) node[below] {C};
            
            \draw            	\circleA circle (1cm) node[anchor=south east] {A};
            \draw             \circleB circle (1cm) node[anchor=south west] {B};
            \draw             \circleC circle (1cm) node[below] {C};
        \end{tikzpicture}
        &
        \begin{tikzpicture}
            \draw (0,0) -- (4,0) -- (4,4) -- (0,4) -- (0,0) node[anchor=south west] {U};
            \begin{scope}
                 \clip \circleA;
                 \fill[fill=pink] \circleB;
                 \fill[fill=pink] \circleC;
            \end{scope}
            
            \draw            	\circleA circle (1cm) node[anchor=south east] {A};
            \draw             \circleB circle (1cm) node[anchor=south west] {B};
            \draw             \circleC circle (1cm) node[below] {C};
        \end{tikzpicture}
        &
        \begin{tikzpicture}
            \draw (0,0) -- (4,0) -- (4,4) -- (0,4) -- (0,0) node[anchor=south west] {U};
            \begin{scope}
                 \clip \circleB;
                 \draw[fill=pink, even odd rule]   \circleB node[anchor=south east] { }
                                                  \circleC;
            \end{scope}
            
            \draw            	\circleA circle (1cm) node[anchor=south east] {A};
            \draw             \circleB circle (1cm) node[anchor=south west] {B};
            \draw             \circleC circle (1cm) node[below] {C};
        \end{tikzpicture}
        \\ \\
        g.      $A'$
        & h.    $(A \cap B)'$
        & i.    $(A \cup B) - C$
        \\        
        \begin{tikzpicture}
            \draw[fill=pink] (0,0) -- (4,0) -- (4,4) -- (0,4) -- (0,0) node[anchor=south west] {U};
            \begin{scope}
                \fill[fill=white] \circleA;
                \fill[fill=white] \circleB;
                \fill[fill=white] \circleC;
            \end{scope}
            
            \draw            	\circleA circle (1cm) node[anchor=south east] {A};
            \draw             \circleB circle (1cm) node[anchor=south west] {B};
            \draw             \circleC circle (1cm) node[below] {C};
        \end{tikzpicture} 
        &      
        \begin{tikzpicture}
            \draw[fill=pink] (0,0) -- (4,0) -- (4,4) -- (0,4) -- (0,0) node[anchor=south west] {U};
             \begin{scope}
                 \clip \circleA;
                 \fill[fill=white] \circleB;
             \end{scope}
            \draw \circleA circle (1cm) node[anchor=south east] {A};
            \draw \circleB circle (1cm) node[anchor=south west] {B};
            \draw \circleC circle (1cm) node[below] {C};
        \end{tikzpicture}
        &      
        \begin{tikzpicture}
            \draw (0,0) -- (4,0) -- (4,4) -- (0,4) -- (0,0) node[anchor=south west] {U};
             \begin{scope}
                 \clip \circleA;{}
                 \draw[fill=pink, even odd rule]   	\circleA node[anchor=south east] {}
                                                    \circleC;
             \end{scope}
             \begin{scope}
                 \clip \circleB;{}
                 \draw[fill=pink, even odd rule]   	\circleB node[anchor=south east] {}
                                                    \circleC;
             \end{scope}
            \draw	\circleA circle (1cm) node[anchor=south east] {A};
            \draw	\circleB circle (1cm) node[anchor=south west] {B};
            \draw 	\circleC circle (1cm) node[below] {C};
        \end{tikzpicture}
    \end{tabular}
  
  \newpage
  \item 
    \begin{enumerate}
      \item   $A \cap (B \cup C) = (A \cap B) \cup (A \cap C)$
        \begin{itemize}
          \item $B \cup C = \{ 1, 2, 3, 4, 5, 6, 8 \}$
          \item $A \cap (B \cup C) = \{ 1, 3, 5 \}$ ~\\
          \item $A \cap B = \{ 1, 3 \}$
          \item $A \cap C = \{1, 5\}$
          \item $(A \cap B) \cup (A \cap C) = \{ 1, 3, 5 \}$ $\checkmark$ 
        \end{itemize} ~\\
    
      \item   $(A \cup B)' = A \cap B'$    
        \begin{itemize}
          \item $A \cup B = \{ 1, 2, 3, 4, 5 \}$
          \item $(A \cup B)' = \{ 6, 7, 8 \}$ ~\\
          \item $A' = \{ 2, 4, 6, 7, 8 \}$
          \item $B' = \{ 5, 6, 7, 8 \}$
          \item $A' \cap B' = \{ 6, 7, 8 \}$ $\checkmark$
        \end{itemize} ~\\
                            
      \item   $A \cap (A \cup B) = A$  
        \begin{itemize}
          \item $A \cup B = \{ 1, 2, 3, 4, 5 \}$
          \item $A \cap (A \cup B) = \{ 1, 3, 5 \}$ ~\\
          \item $A = \{ 1, 3, 5 \}$ $\checkmark$
        \end{itemize} ~\\
                                 
    \end{enumerate}
    
  \item ~\\
    Step 1: $x \in \mathbb{Z}$ ~\\
    Step 2: $x$ is an odd integer ~\\
    Step 3: $x = 2k+1$ ~\\
    Step 4: $\{ x \in \mathbb{Z} : x = 2k+1$ for some $k \in \mathbb{Z} \}$
    
  \item ~\\
    Step 1: $x \in \mathbb{Z}$ ~\\
    Step 2: $x$ is divisible by 3 ~\\
    Step 3: $x = 3k$ ~\\
    Step 4: $\{ 3k : k \in \mathbb{Z} \}$
\end{enumerate}
}


\input{BASE-FOOT}


