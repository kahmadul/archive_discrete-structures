% --- ASSIGNMENT INFO --- %
\newcommand{\laClass}       {CS 210}
\newcommand{\laTopic}       {Conditional Statements (Implications)}
\newcommand{\laKey}         {U1E2}

\input{BASE-HEAD}


% -------------------------------------------------------------------- %
\begin{intro}{Conditional Statements (Implications)}
    A statement like ``if $p$ is true, then $q$ is true" is known
    as an implication. It can be written symbolically as $p \to q$,
    and read as ``p implies q" or ``if p, then q".
    For the implication $p \to q$, $p$ is the \textbf{hypothesis}
    and $q$ is the \textbf{conclusion}.
    ~\\

    The \textbf{hypothesis} and the \textbf{conclusion} can be made of
    compound statements as well,
    such as $(p \lor q) \to (r \land s)$.


    \paragraph{Example:} Write the following statement symbolically,
    defining the propositional variable:

    ``If it is Kate's birthday, then Kate will get a cake."

    ~\\ I will define two propositional variables:
    ~\\ \tab $b$ is ``It is Kate's birthday"
    ~\\ \tab $c$ is ``Kate will get a cake."

    ~\\
    And then I can write it symbolically: $b \to c$.
\end{intro}


\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
  For the following statements, create two
  \textbf{propositional variables}, assign them values (from
  the original statement), and then write it symbolically.

  \begin{enumerate}
      \item[(a)]  IF you eat breakfast, THEN you will have energy.
                  \vspace{3.5cm}
      \item[(b)]  IF Timmy's age is over 8 AND Timmy's age is less than 13, THEN Timmy gets Tween-priced movie tickets.
                  \vspace{3.5cm}
      \item[(c)]  IF I get enough sleep, OR I drink coffee, THEN I can work.
                  \vspace{3.5cm}
      \item[(d)]  IF you don't play, THEN you can't win!
                  \vspace{3cm}
  \end{enumerate}
\end{questionNOGRADE}

\newpage
\begin{intro}{Truth for implications} \footnotesize
    With an implication, ``if \textbf{hypothesis}, then \textbf{conclusion}",
    the truth table will look a bit different than what you've done so far.
    Make sure to take notice because it is easy to make an error when it
    comes to implication truth tables!

    For an implication to be logically FALSE, it must be the case that
    the \textbf{hypothesis is true, but the conclusion is false}. In
    all other cases, the implication results to \textbf{true}.

    \begin{center}
        \begin{tabular}{| c | c | | c |}
            \hline{}
            $p$     & $q$     & $p \to q$   \\ \hline
            \true   & \true   & \true       \\ \hline
            \true   & \false  & \false      \\ \hline
            \false  & \true   & \true       \\ \hline
            \false  & \false  & \true       \\ \hline
        \end{tabular}
    \end{center}

    Seems weird? Think of it this way: The only FALSE result is if
    the \textbf{hypothesis} is true, but the \textbf{conclusion}
    ends up being false. In science, it would mean our hypothesis
    has been disproven. If, however, the \textbf{hypothesis}
    is false, we can't really say anything about the conclusion
    (again thinking about science experiments)

    \paragraph{Running an experiment}
    A scientist is testing the statement,
    ``A watched pot never boils'', and they have formulated the following implication:
    $w \to \neg b$: ~\\
    \textit{If \underline{you watch the pot} then \underline{the water won't boil}}.

    \begin{center}
      \begin{tabular}{c | l l}
        \textbf{Scenario} & \textbf{Hypothesis}       & \textbf{Conclusion}     \\ \hline
        1                 & You watch the pot         & The water doesn't boil  \\ \hline
        2                 & You watch the pot         & The water does boil     \\ \hline
        3                 & You don't watch the pot   & The water doesn't boil  \\ \hline
        4                 & You don't watch the pot   & The water does boil
      \end{tabular}
    \end{center}

    \begin{itemize}
      \item   For scenario 1, the scientist watches the pot, and the water doesn't boil.
              They've tested their \textbf{hypothesis} and the conclusion came out to true.
              The experiment is valid and the result of $w \to \neg b$ is \truetext

      \item   For scenario 2, the scientist watches the pot, and the water DOES boil.
              They've tested their \textbf{hypothesis} and the conclusion came out to false.
              The experiment is valid and the result of $w \to \neg b$ is \falsetext

      \item   For scenarios 3 and 4, the scientist walked away from the pot - they
              didn't even perform the \textbf{hypothesis} correctly, so whether or not the
              water boiled, the experiment was not proven or disproven! Therefore,
              the result of the implication is \truetext , because it is not disproven.
    \end{itemize}
\end{intro} \normalsize

\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
Complete the truth tables for the following compound expressions.

\begin{enumerate}
  \item[(a)]  $(p \land q) \to r$ ~\\
          \large
          \begin{tabular}{ | c | c | c || c || c | } \hline
            $p$     & $q$     & $r$     & $p \land q$   & $(p \land q) \to r$ \\ \hline
            \true   & \true   & \true   &               & \true               \\ \hline
            \true   & \true   & \false  &               &                     \\ \hline
            \true   & \false  & \true   & \false        &                     \\ \hline
            \true   & \false  & \false  & \false        &                     \\ \hline
            \false  & \true   & \true   &               &                     \\ \hline
            \false  & \true   & \false  &               &                     \\ \hline
            \false  & \false  & \true   & \false        &                     \\ \hline
            \false  & \false  & \false  & \false        & \true               \\ \hline
          \end{tabular}
          \normalsize

  \item[(b)] $p \to (q \lor r)$ ~\\
          \large
          \begin{tabular}{ | c | c | c || c || c | } \hline
            $p$     & $q$     & $r$     & $q \lor r $   & $p \to (q \lor r)$  \\ \hline
            \true   & \true   & \true   & \true         & \true              \\ \hline
            \true   & \true   & \false  &               &                     \\ \hline
            \true   & \false  & \true   &               &                     \\ \hline
            \true   & \false  & \false  & \false        &                     \\ \hline
            \false  & \true   & \true   &               &                     \\ \hline
            \false  & \true   & \false  &               &                     \\ \hline
            \false  & \false  & \true   &               &                     \\ \hline
            \false  & \false  & \false  & \false        & \true               \\ \hline
          \end{tabular}
          \normalsize
\end{enumerate}
\end{questionNOGRADE}

\newpage
\begin{intro}{Negations of Implications}
  The negation of the implication $p \to q$ is the statement
  $p \land (\neg q)$. We can see that these are equivalent
  with a truth table:

  \begin{center}
      \begin{tabular}{| c | c || c || c || c |} \hline
          $p$     & $q$     & $p \to q$   & $\neg( p \to q)$  & $p \land \neg q$    \\ \hline
          \true   & \true   & \true       & \false            & \false              \\ \hline
          \true   & \false  & \false      & \true             & \true               \\ \hline
          \false  & \true   & \true       & \false            & \false              \\ \hline
          \false  & \false  & \true       & \false            & \false              \\ \hline
      \end{tabular}
  \end{center}

  It is important to remember that \textbf{the negation of an
  implication is not also an implication!}


  \paragraph{Let's look at our experiment again:} ~\\
    \textit{If \underline{you watch the pot} then \underline{the water won't boil}}.

    ~\\
    For an easier time reading it, let's make the variables this time:
    ~\\ You watch the pot,
    ~\\ The water \textit{doesn't} boil.
    ~\\ So we're writing the implication as $p \to q$ this time.
    ~\\

    And remember our scenarios:

    \begin{center}
      \begin{tabular}{c | l l}
        \textbf{Scenario} & \textbf{Hypothesis}       & \textbf{Conclusion}     \\ \hline
        1                 & You watch the pot         & The water doesn't boil  \\ \hline
        2                 & You watch the pot         & The water does boil     \\ \hline
        3                 & You don't watch the pot   & The water doesn't boil  \\ \hline
        4                 & You don't watch the pot   & The water does boil
      \end{tabular}
    \end{center}

    The only one that actually \textit{disproves} the experiment is scenario 2, where you
    watch the pot \textit{and} it does not boil.

    ~\\
    We \textbf{performed the hypothesis} ($p$), AND
    the conclusion did not occur ($\neg q$),
    meaning that the result is $p \land \neg q$.

    ~\\
    If the experiment verified the result, that would be $p \to q$.
    ~\\
    Since the experiment failed, that means $\neg ( p \to q )$.
    
    ~\\~\\
    Using our truth tables, we can see that $\neg (p \to q) \equiv p \land \neg q$.
\end{intro}

\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
For each of the following statements, write out the negation both symbolically and in English.


\begin{enumerate}
    \item[(a)] $m$: you watch this movie, $c$: you cry. \\  $m \to c$: If you watch this movie, then you cry.
              ~\\ \solution{ $\neg( m \to c ) \equiv m \land \neg c$: You watch this movie and you do not cry. }{}
    \item[(b)]
        $j$: Jessica gets chocolate,
        $c$: Jessica gets cake, \\
        $b$: Jessica has a happy birthday. \\
        $(j \lor c) \to b$: If Jessica gets chocolate or Jessica gets cake, then Jessica has a happy birthday.
        
        \vspace{5cm}
        
    \item[(c)]
        $n$: You like Nintendo, $m$: You like Mario games, \\
        $z$: You like Zelda games. \\
        $n \to (m \lor z)$: If you like Nintendo, then you like Mario games or you like Zelda games.
            
        \vspace{4cm}
\end{enumerate}    
\end{questionNOGRADE}

\newpage
\begin{intro}{Converse} \footnotesize
  Given 
  $p$: Ice cream is in the fridge, $q$: You eat dessert,
  and the implication $p \to q$ (if ice cream is in the fridge then you eat dessert),
  
  ~\\
  The \textbf{Converse} is $q \to p$, 
  which would be ``If you have dessert then there is ice cream in the fridge.''
  
  \begin{center}
      \begin{tabular}{| c | c | | c || c |} \hline
          hypothesis  & conclusion  & implication & converse \\ 
          $p$         & $q$         & $p \to q$   & $q \to p$     \\ \hline
          \true       & \true       & \true       & \true         \\ \hline
          \true       & \false      & \false      & \true         \\ \hline
          \false      & \true       & \true       & \false        \\ \hline
          \false      & \false      & \true       & \true         \\ \hline
      \end{tabular}
  \end{center}
\end{intro}

\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
For each of the following statements, write out the \underline{converse}
 both symbolically and in English.

\begin{enumerate}
    \item[(a)] $m$: you watch this movie, $c$: you cry. \\  $m \to c$: If you watch this movie, then you cry.
        \vspace{4cm}
    \item[(b)]
        $n$: You like Nintendo, $m$: You like Mario games, \\
        $z$: You like Zelda games. \\
        $n \to (m \lor z)$: If you like Nintendo, then you like Mario games or you like Zelda games.
\end{enumerate} 
\end{questionNOGRADE}


\newpage
\begin{intro}{Inverse} \footnotesize
  Given 
  $p$: Ice cream is in the fridge, $q$: You eat dessert,
  and the implication $p \to q$ (if ice cream is in the fridge then you eat dessert),
  
  ~\\
  The \textbf{Inverse} is $\neg p \to \neg q$, 
  which would be ``If there is no ice cream in the fridge then you don't have dessert.''
  
  \begin{center}
      \begin{tabular}{| c | c | | c || c |} \hline
          hypothesis  & conclusion  & implication     & inverse               \\ 
          $p$         & $q$         & $p \to q$       & $\neg p \to \neg q$   \\ \hline
          \true       & \true       & \true           & \true                 \\ \hline
          \true       & \false      & \false          & \true                 \\ \hline
          \false      & \true       & \true           & \false                \\ \hline
          \false      & \false      & \true           & \true                 \\ \hline
      \end{tabular}
  \end{center}

\end{intro}
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
For each of the following statements, write out the \underline{inverse}
 both symbolically and in English.

\begin{enumerate}
    \item[(a)] $m$: you watch this movie, $c$: you cry. \\  $m \to c$: If you watch this movie, then you cry.
      \vspace{3cm}
    \item[(b)]
        $n$: You like Nintendo, $m$: You like Mario games, \\
        $z$: You like Zelda games. \\
        $n \to (m \lor z)$: If you like Nintendo, then you like Mario games or you like Zelda games.
            
\end{enumerate} 

\end{questionNOGRADE}

\newpage

\begin{intro}{Contrapositive} \footnotesize
  Given 
  $p$: Ice cream is in the fridge, $q$: You eat dessert,
  and the implication $p \to q$ (if ice cream is in the fridge then you eat dessert),
  
  ~\\
  The \textbf{Contrapositive} is $\neg q \to \neg p$, 
  which would be ``If you don't eat dessert then you don't have ice cream in the fridge''
  
  \begin{center}
      \begin{tabular}{| c | c | | c || c |} \hline
          hypothesis  & conclusion  & implication     & contrapositive        \\ 
          $p$         & $q$         & $p \to q$       & $\neg q \to \neg p$   \\ \hline
          \true       & \true       & \true           & \true                 \\ \hline
          \true       & \false      & \false          & \false                \\ \hline
          \false      & \true       & \true           & \true                 \\ \hline
          \false      & \false      & \true           & \true                 \\ \hline
      \end{tabular}
  \end{center}
  
  Notice that the truth tables for $p \to q$ and $\neg q \to \neg p$ are logically equivalent!
\end{intro}

\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
For each of the following statements, write out the \underline{contrapositive} 
 both symbolically and in English.

\begin{enumerate}
    \item[(a)] $m$: you watch this movie, $c$: you cry. \\  $m \to c$: If you watch this movie, then you cry.
      \vspace{3cm}
    \item[(b)]
        $n$: You like Nintendo, $m$: You like Mario games, \\
        $z$: You like Zelda games. \\
        $n \to (m \lor z)$: If you like Nintendo, then you like Mario games or you like Zelda games.
            
\end{enumerate} 

\end{questionNOGRADE}

% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
\newpage
\section*{Solutions}
\solution{

\begin{enumerate}
    \item
    \begin{enumerate}
        \item   \textbf{IF you eat breakfast, THEN you have energy.}
                ~\\   $b$: You eat breakfast, $e$: You have energy.
                ~\\   $b \to e$
        \item   \textbf{IF Timmy's age is over 12 AND Timmy has a student ID, THEN Timmy gets cheap movie tickets.}
                ~\\   $a$: Timmy's age is over 12, $s$: Timmy has a student ID, $c$: Timmy gets cheap movie tickets.
                ~\\   $(a \land s) \to c$
        \item   \textbf{IF I get enough sleep, OR I drink coffee, THEN I can work.}
                ~\\   $s$: I get enough sleep, $c$: I drink coffee, $w$: I can work.
                ~\\   $(s \lor c) \to w$
        \item   \textbf{IF you don't play, THEN you can't win!}
                ~\\   $p$: You play, $w$: You can win
                ~\\   $\neg p \to \neg w$
                ~\\   (It's better to keep the expressions phrased ``positively'', to avoid double negatives.)
    \end{enumerate}



    \item
    \begin{enumerate}
      \item   $(p \land q) \to r$ ~\\
              \large
              \begin{tabular}{ | c | c | c || c || c | } \hline
                $p$     & $q$     & $r$     & $p \land q$   & $(p \land q) \to r$ \\ \hline
                \true   & \true   & \true   & T             & \true               \\ \hline
                \true   & \true   & \false  & T             & F                   \\ \hline
                \true   & \false  & \true   & \false        & T                   \\ \hline
                \true   & \false  & \false  & \false        & T                   \\ \hline
                \false  & \true   & \true   & F             & T                   \\ \hline
                \false  & \true   & \false  & F             & T                   \\ \hline
                \false  & \false  & \true   & \false        & T                   \\ \hline
                \false  & \false  & \false  & \false        & \true               \\ \hline
              \end{tabular}
              \normalsize
      \newpage
      \item   $p \to (q \lor r)$ ~\\
              \large
              \begin{tabular}{ | c | c | c || c || c | } \hline
                $p$     & $q$     & $r$     & $q \lor r $   & $p \to (q \lor r)$  \\ \hline
                \true   & \true   & \true   & \true         & \true              \\ \hline
                \true   & \true   & \false  & T             & T                   \\ \hline
                \true   & \false  & \true   & T             & T                   \\ \hline
                \true   & \false  & \false  & \false        & F                   \\ \hline
                \false  & \true   & \true   & T             & T                   \\ \hline
                \false  & \true   & \false  & T             & T                   \\ \hline
                \false  & \false  & \true   & T             & T                   \\ \hline
                \false  & \false  & \false  & \false        & \true               \\ \hline
              \end{tabular}
              \normalsize
    \end{enumerate}



    \item   
      \begin{enumerate}
          \item[(a)] $m$: you watch this movie, $c$: you cry. \\  $m \to c$: If you watch this movie, then you cry.
                    ~\\ \solution{ $\neg( m \to c ) \equiv m \land \neg c$: You watch this movie and you do not cry. }{}
                    
          \vspace{1cm}
          \item[(b)]
              $j$: Jessica gets chocolate,
              $c$: Jessica gets cake, \\
              $b$: Jessica has a happy birthday. \\
              $(j \lor c) \to b$: If Jessica gets chocolate or Jessica gets cake, then Jessica has a happy birthday.
              ~\\
              $\neg ( (j \lor c) \to b ) \equiv (j \lor c) \land \neg b$: Jessica gets chocolate or chocolate gets cake, and Jessica does not have a happy birthday.
            
            \vspace{1cm}
            \item[(c)]
                $n$: You like Nintendo, $m$: You like Mario games, \\
                $z$: You like Zelda games. \\
                $n \to (m \lor z)$: If you like Nintendo, then you like Mario games or you like Zelda games.                  
                ~\\
                $ \neg ( n \to (m \lor z) ) \equiv n \land \neg ( m \lor z ) \equiv n \land \neg m \land \neg z )$:
                You like nintendo and you don't like Mario games and you don't like Zelda games.
                ~\\
                (Maybe you just prefer Metroid.)
      \end{enumerate}    
    
    \newpage
    \item   Converse \footnote{The converse of $p \to q$ is $q \to p$.} ~\\
      \begin{enumerate}
          \item[(a)] $m$: you watch this movie, $c$: you cry. \\  $m \to c$: If you watch this movie, then you cry.
              ~\\ $c \to m$: If you cry then you watch the movie.              
              
          \item[(b)]
              $n$: You like Nintendo, $m$: You like Mario games, \\
              $z$: You like Zelda games. \\
              $n \to (m \lor z)$: If you like Nintendo, then you like Mario games or you like Zelda games.
              ~\\ $(m \lor z) \to n$: If you like Mario games or you like Zelda games, then you like Nintendo.
      \end{enumerate} 


    \item   Inverse \footnote{The inverse of $p \to q$ is $\neg p \to \neg q$.}
          
      \begin{enumerate}
          \item[(a)] $m$: you watch this movie, $c$: you cry. \\  $m \to c$: If you watch this movie, then you cry.
              ~\\ $\neg m \to \neg c$: If you don't watch the movie then you don't cry.
          \item[(b)]
              $n$: You like Nintendo, $m$: You like Mario games, \\
              $z$: You like Zelda games. \\
              $n \to (m \lor z)$: If you like Nintendo, then you like Mario games or you like Zelda games.
              ~\\ $\neg n \to \neg (m \lor z) \equiv \neg n \to \neg m \land \neg z$:
              If you do not like Nintendo, then you don't like Mario games and you don't like Zelda games.
      \end{enumerate} 


    \item   Contrapositive \footnote{The contrapositive of $p \to q$ is $\neg q \to \neg p$.}
          
      \begin{enumerate}
          \item[(a)] $m$: you watch this movie, $c$: you cry. \\  $m \to c$: If you watch this movie, then you cry.
              ~\\ $\neg c \to \neg m$: If you don't cry then you don't watch the movie.
          \item[(b)]
              $n$: You like Nintendo, $m$: You like Mario games, \\
              $z$: You like Zelda games. \\
              $n \to (m \lor z)$: If you like Nintendo, then you like Mario games or you like Zelda games.
              ~\\ $\neg ( m \lor z ) \to \neg n \equiv \neg m \land \neg z \to \neg n$:
              If you don't like Mario games and you don't like Zelda games then you don't like Nintendo.
      \end{enumerate} 
\end{enumerate}

}{}
\input{BASE-FOOT}


