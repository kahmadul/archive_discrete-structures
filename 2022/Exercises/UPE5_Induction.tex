% --- ASSIGNMENT INFO --- %
\newcommand{\laClass}       {CS 210}
\newcommand{\laTopic}       {Inductive proofs}
\newcommand{\laKey}         {UPE5}

\input{BASE-HEAD}

% -------------------------------------------------------------------- %
\begin{intro}{Closed formulas for number sequences}
  A closed formula is a formula that takes some input $n$, and based
  on the position we plug in for $n$, we get the next number in a sequence
  of numbers.
  
  ~\\
  For example, let's say we have the formula $$a_{n} = 2n$$
  
  ~\\ We can plug in different values of $n$ to find each item in the sequence:
  
  \begin{center}
    \begin{tabular}{l l l}
      \textbf{Index}  & \textbf{Plugged in} & \textbf{Result} \\ \hline
      1               & $a_{1} = 2(1)$      & The 1st item is 2 \\
      2               & $a_{2} = 2(2)$      & The 2nd item is 4 \\
      3               & $a_{3} = 2(3)$      & The 3rd item is 6 \\
      4               & $a_{4} = 2(4)$      & The 4th item is 8 \\
    \end{tabular}
  \end{center}
  
  So part of the sequence is $$2, 4, 6, 8$$
  Of course, this sequence goes on forever, but we can find a value for any position
  since we have the formula.
\end{intro}
% -------------------------------------------------------------------- %
\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
  Find the first 5 items in the sequence for each formula given,
  with the first index being $n=1$.
  
  \begin{enumerate}
    \item[(a)]    $a_{n} = n + 2$           \vspace{3.5cm}
    \item[(b)]    $b_{n} = 3n + 1$          \vspace{3.5cm}
    \item[(c)]    $c_{n} = 2n - 1$          \vspace{3.5cm}
    \item[(d)]    $d_{n} = n^{2} + 4n + 4$  \vspace{3.5cm}
  \end{enumerate}
\end{questionNOGRADE}
% -------------------------------------------------------------------- %
\newpage
\begin{intro}{Recursive formulas (aka recurrence relation) for number sequences}
  With a recursive formula, we must give at least one starting value, such as
  $$a_{1} = 1$$ and subsequent values in the sequence are based off each
  preceeding element, such as $$a_{n} = 2n_{n-1} + 1$$
  so we can find each element value by finding the values for each previous element:
  
  \begin{center}
    \begin{tabular}{l l l}
      \textbf{Index}  & \textbf{Plugged in} & \textbf{Result} \\ \hline
      1               & $a_{1} = 1$                             & The first term is given \\
      2               & $a_{2} = 2a_{1} + 1 = 2(1)+1 = 3$       & The 2nd item is 3 \\
      3               & $a_{3} = 2a_{2} + 1 = 2(3)+1 = 7$       & The 3rd item is 7 \\
      4               & $a_{4} = 2a_{3} + 1 = 2(7)+1 = 15$      & The 4th item is 15 \\
    \end{tabular}
  \end{center}
\end{intro}
% -------------------------------------------------------------------- %
\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
  Find the subsequent 4 items in the sequence for each formula given.
  
  \begin{enumerate}
    \item[(a)]    $a_{1} = 1$ \tab $a_{n} = a_{n-1} + 1$                    \vspace{3.5cm}
    \item[(b)]    $b_{1} = 2$ \tab $b_{n} = 2a_{n-1}$                       \vspace{3.5cm}
    \item[(c)]    $c_{1} = 1$ \tab $c_{n} = 3a_{n-1} + 2$                   \vspace{3.5cm}
    \item[(d)]    $d_{1} = 1, d_{2} = 1$ \tab $d_{n} = d_{n-1} + d_{n-2}$   \vspace{3.5cm}
  \end{enumerate}
\end{questionNOGRADE}
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
\newpage
\begin{intro}{Summations}
  For a sequence of numbers (denoted $a_{k}$, where $k >= 1$,
  we can use the notation
  $$\sum_{k=1}^{n} a_{k}$$
  to denote the sum of the first $n$ terms of the sequence.
  This is called \textit{sigma notation}.

  \paragraph{Example:} Evaluate the sum $\sum_{k=1}^{3}(2k-1)$. ~\\
  First, we need to find the elements at $k=1$, $k=2$, and $k=3$:

  \begin{center}
      \begin{tabular}{| c | c | c |}
          \hline
          \textbf{ $k=1$ } & \textbf{ $k=2$ } & \textbf{ $k=3$ } \\
          \hline
          $a_{1} = (2 \cdot 1 - 1) = 1$ &
          $a_{2} = (2 \cdot 2 - 1) = 3$ &
          $a_{3} = (2 \cdot 3 - 1) = 5$
          \\
          \hline
      \end{tabular}
  \end{center}
  ~\\
  Then, we can add the values: \\
  $\sum_{k=1}^{3}(2k-1) $ \tab
  $= a_{1} + a_{2} + a_{3}$ \tab
  $= 1 + 3 + 5 $ \tab
  \fbox{ $= 9$ }
\end{intro}
% -------------------------------------------------------------------- %
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
  Evaluate the following summations.

  \begin{center}
      \begin{tabular}{p{6cm} p{6cm}}
      a. $$ \sum_{k=1}^{4}(3k) $$
      &
      b. $$ \sum_{k=1}^{5}(4) $$
  \end{tabular} 
  \end{center}     
\end{questionNOGRADE}
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
\newpage
\begin{intro}{1. Recursive / Closed formula equivalence}
  Show that the sequence defined by $a_{k} = a_{k-1} + 4; a_{1} = 1$ for $k \geq 2$
  is equivalently described by the closed formula $a_{n} = 4n - 3$.
  \footnote{From Discrete Mathematics, Ensley and Crawley, pg 121, question 3a}

  \paragraph{Step 1: Check $a_{1}$ for both formulas.} ~\\
      Recursive: $a_{1} = 1$ (provided); \tab{}
      Closed: $a_{1} = 4(1) - 3 = 1$  \tab \checkmark OK

  \paragraph{Step 2: Rewrite the recursive formula in terms of $m$:}
  $$ a_{m} = a_{m-1} + 4 $$

  \paragraph{Step 3: Find the equation for $a_{m-1}$ via the closed formula:}
  $$ a_{n} = 4n-3; \tab a_{m-1} = 4(m-1) - 3; \tab a_{m-1} = 4m-7 $$

  \paragraph{Step 4: Plug $a_{m-1}$ back into the recursive formula and simplify.}
  $$ a_{m} = a_{m-1} + 4; \tab \to \tab a_{m} = (4m-7) + 4 $$
  $$ a_{m} = 4m - 7 + 4; \tab a_{m} = 4m - 3 $$

  Looking back at the closed formula, $a_{n} = 4n - 3$, our result from
  Step 4 and this match, so we have proven that, for all values $k \geq 2$,
  the recursive formula and closed formula give the same sequence.
\end{intro}
% -------------------------------------------------------------------- %
\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
  Show that the sequence defined by
  $a_{n} = a_{n-1} + 2; a_{1} = 5$ for $k \geq 2$ is equivalently
  described by the closed formula, $a_{n} = 2n+3$.

\paragraph{Step 1: Check $a_{1}$ for both formulas.} ~\\
    \vspace{1cm}
    
\paragraph{Step 2: Rewrite the recursive formula in terms of $m$:} ~\\
    \vspace{1cm}

\paragraph{Step 3: Find the equation for $a_{m-1}$ via the closed formula:} ~\\
    \vspace{2cm}

\paragraph{Step 4: Plug $a_{m-1}$ back into the recursive formula and simplify.} ~\\
    \vspace{3cm}
\end{questionNOGRADE}
% -------------------------------------------------------------------- %
\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}  
    Show that the sequence defined by
    $ a_{k} = 2 \cdot a_{k-1} + 1 ; a_{1} = 1 $
    for $k \geq 2$ is equivalently
    described by the closed formula,
    $ a_{n} = 2^{n} - 1 $

  \paragraph{Step 1: Check $a_{1}$ for both formulas.} ~\\
    \vspace{1cm}
      
  \paragraph{Step 2: Rewrite the recursive formula in terms of $m$:} ~\\
    \vspace{1cm}

  \paragraph{Step 3: Find the equation for $a_{m-1}$ via the closed formula:} ~\\
    \vspace{2cm}

  \paragraph{Step 4: Plug $a_{m-1}$ back into the recursive formula and simplify.} ~\\
    \vspace{4cm}
      
  \begin{hint}{Exponent rules}
      \textbf{Power rule:} $(a^{m})^{n} = a^{mn}$ \\
      \textbf{Negative exponent rule:} $a^{-n} = \frac{1}{a^{n}}$ \\
      \textbf{Product rule:} $a^{m} \cdot a^{n} = a^{m+n}$ \\
      \textbf{Quotient rule:} $\frac{a^{m}}{a^{n}} = a^{m-n}$
  \end{hint}
\end{questionNOGRADE}
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
\begin{intro}{2. Summation / Closed formula equivalence}
  Another type of proof we will do is to show that, for some $n$ plugged
  into a sum and into a closed formula for that value.

  Use induction to prove the proposition. As part of the proof,
  verify the statement for $n = 1$, $n = 2$, and $n = 3$.
  $ \sum_{i=1}^{n} (2i - 1) = n^{2} $        for each $n \geq 1$.
  \footnote{From Discrete Mathematics, Ensley and Crawley, pg 122, question 8a}

  \paragraph{Step 1: Show that the proposition is true for 1, 2, and 3. } ~\\

      \begin{tabular}{l | p{4cm} | p{4cm} }
          \textbf{ $i$ value } &
          \textbf{ $ \sum_{i=1}^{n} (2i - 1) $ } &
          \textbf{ $n^{2}$ }
          \\ \hline
          $i = 1$ &
              $ \sum_{i=1}^{1} (2i - 1) $ &
              $1^{2}$
              \\
              &
              $= ( 2 \cdot 1 - 1 ) = 1$ &
              $= 1$ \tab\checkmark
          \\ && \\
          $i = 2$ &
              $ \sum_{i=1}^{2} (2i - 1) $&
              $2^{2}$
              \\
              &
              $ = (2 \cdot 1 - 1) + (2 \cdot 2 - 1)$ &
              
              \\
              &
              $= (1) + (3) = 4$ &
              $ = 4$ \tab\checkmark
          \\ && \\
          $i = 3$ &
              $ \sum_{i=1}^{3} (2i - 1) $&
              $3^{2}$
              \\
              &
              $ = 1 + 3 + (2 \cdot 3 - 1)$
              &
              \\
              &
              $ = 1 + 3 + 5 = 9 $ &
              $ = 9 $  \tab\checkmark               
      \end{tabular}
      
  \paragraph{Step 2: Rewrite the summation as $ \sum_{i=1}^{m-1} (2i - 1) + (2m - 1) $:}

  ~\\~\\
  $ \sum_{i=1}^{m}(2i-1) = \sum_{i=1}^{m-1}(2i-1) + (2m - 1)$

  \paragraph{Step 3: Find an equation  for $\sum_{i=1}^{m-1}$ via the proposition: }

  ~\\~\\
  $ \sum_{i=1}^{n} (2i - 1) = n^{2} \tab ... \sum_{i=1}^{m-1}(2i-1) = (m-1)^{2} $
  
  \paragraph{Step 4: Plug $\sum_{i=1}^{m-1}$ into the summation from step (2):}

  ~\\~\\
  $ \sum_{i=1}^{m}(2i-1) = \sum_{i=1}^{n-1}(2i-1) + (2m - 1) \tab = (m-1)^{2} + (2m - 1)$ \\
  $ \sum_{i=1}^{m}(2i-1) =  m^{2} - 2m + 1 + 2m - 1 $ \\
  $ \sum_{i=1}^{m}(2i-1) =  m^{2} $ \tab \checkmark{}

  ~\\
  We get the same form as the original proposition, proving our statement.
\end{intro}
% -------------------------------------------------------------------- %
\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
  Use induction to prove
  $$ \sum_{i=1}^{n} (2i+4) = n^{2} + 5n$$
  for each $n \geq 1$.
  \footnote{From Discrete Mathematics, Ensley and Crawley, pg 122 question 8b}

  \paragraph{Step 1: Show that the proposition is true for 1, 2, and 3. } ~\\
      \begin{tabular}{l | p{4cm} | p{4cm} }
          \textbf{ $i$ value } &
          \textbf{ $ \sum_{i=1}^{n} (2i+4) $ } &
          \textbf{ $n^{2} + 5n$ }
          \\ \hline
          $i = 1$ & &

          \\ && \\
          $i = 2$ & &

          \\ && \\
          $i = 3$ & &
      \end{tabular}

  \paragraph{Step 2: Rewrite the summation:} ~\\ \vspace{1cm}
  
  \paragraph{Step 3: Find an equation  for $\sum_{i=1}^{m-1}$ via the proposition: } ~\\ \vspace{2cm}

  \paragraph{Step 4: Plug $\sum_{i=1}^{m-1}$ into the summation from step 2 and simplify:} ~\\ \vspace{2cm}
\end{questionNOGRADE}
% -------------------------------------------------------------------- %
\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
  Use induction to prove that for every positive integer $n$,
  $$ \sum_{i=1}^{n} i = \frac{n(n+1)}{2}$$
  
  \paragraph{Step 1: Show that the proposition is true for 1, 2, and 3. } ~\\
      \begin{tabular}{l | p{4cm} | p{4cm} }
          \textbf{ $i$ value } &
          \textbf{ $ \sum_{i=1}^{n} (2i+4) $ } &
          \textbf{ $n^{2} + 5n$ }
          \\ \hline
          $i = 1$ & &

          \\ && \\
          $i = 2$ & &

          \\ && \\
          $i = 3$ & &
      \end{tabular}

  \paragraph{Step 2: Rewrite the summation:} ~\\ \vspace{1cm}
  
  \paragraph{Step 3: Find an equation  for $\sum_{i=1}^{m-1}$ via the proposition: } ~\\ \vspace{2cm}

  \paragraph{Step 4: Plug $\sum_{i=1}^{m-1}$ into the summation from step 2 and simplify:} ~\\ \vspace{2cm}
\end{questionNOGRADE}
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
\newpage
\begin{intro}{3. Sums as Recursive sequences}
  Previously we were proving that a sum and a closed formula
  were equivalent. Now we will use induction to prove that
  a sum and a recursive sequence are equivalent.
  Again, we have a set of steps that you'll need to follow
  in order to solve these.

  \paragraph{Example 1 from the textbook}
  Consider the sum $\sum_{i=1}^{n}(2i-1)$, which is the same
  as $1 + 3 + 5 + ... + (2n-1)$. Use the notation $s_{n}$ to
  denote this sum. Find a recursive description of $s_{n}$.

  \subparagraph{Step 1: Find the first term, $s_{1}$:}
      We solve for $s_{1}$, or in other words, $\sum_{i=1}^{1}(2i-1)$.
      
      $\sum_{i=1}^{1}(2i-1) = (2 \cdot 1 - 1) = 1$ \tab
      $s_{1} = 1$

  \subparagraph{Step 2: Restate the result of $s_{n}$ as $s_{n-1}$ plus the final term:}
      Similar to last time, we were rewriting some $\sum_{i=1}^{n}$
      as the sum up until $n-1$, plus the final term. Here,
      we're using $s_{n}$ to represent this sum from $i=1$ to $n$,
      so we can rewrite it as:

      $$ s_{n} = s_{n-1} + (2n-1)$$

  So we have found an equation and the first term, and we can
  show that the recursive formula that is equivalent is:

  $$ s_{1} = 1 \tab s_{n} = s_{n-1} + 2n - 1 $$
\end{intro}
% -------------------------------------------------------------------- %
\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
  Consider the sum $\sum_{i=1}^{n} (3i^{2})$. Use the notation
  $s_{n}$ to denote this sum. Find a recursive description of $s_{n}$.

  \paragraph{Step 1: Find $s_{1}$}

  ~\\~\\~\\~\\

  ~\\~\\~\\~\\

  \paragraph{Step 2: Rewrite $s_{n}$ in terms of $s_{n-1}$ plus final term}
  
  ~\\~\\~\\~\\

  ~\\~\\~\\~\\

  \paragraph{So, the recursive formula is:} ~\\~\\
    $s_{1} = $ 
    \tab[4cm]
    $s_{n} = $ 
\end{questionNOGRADE}
% -------------------------------------------------------------------- %
\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
  Consider the sum $\sum_{i=1}^{n} (2^{i-1}+1)$. Use the notation
  $s_{n}$ to denote this sum. Find a recursive description of $s_{n}$.

  \paragraph{Step 1: Find $s_{1}$}

  ~\\~\\~\\~\\

  ~\\~\\~\\~\\

  \paragraph{Step 2: Rewrite $s_{n}$ in terms of $s_{n-1}$ plus final term}
  
  ~\\~\\~\\~\\

  ~\\~\\~\\~\\

  \paragraph{So, the recursive formula is:} ~\\~\\
      $s_{1} = $ 
      \tab[4cm]
      $s_{n} = $
  
\end{questionNOGRADE}
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %

\newpage
\section*{Solutions}
\solution{

\begin{enumerate}
  \item   Finding values from closed formulas
    \begin{enumerate}
      \item[(a)]    $a_{n} = n + 2$                                           ~\\
                    $a_{1} = 1 + 2 = 3$                                       ~\\
                    $a_{2} = 2 + 2 = 4$                                       ~\\
                    $a_{3} = 3 + 2 = 5$                                       ~\\
                    $a_{4} = 4 + 2 = 6$                                       ~\\
                    $a_{5} = 5 + 2 = 7$                                       ~\\

      \item[(b)]    $b_{n} = 3n + 1$                                          ~\\
                    $b_{1} = 3(1) + 1 = 3 + 1 = 4$                            ~\\
                    $b_{2} = 3(2) + 1 = 6 + 1 = 7$                            ~\\
                    $b_{3} = 3(3) + 1 = 9 + 1 = 10$                           ~\\
                    $b_{4} = 3(4) + 1 = 12 + 1 = 13$                          ~\\
                    $b_{5} = 3(5) + 1 = 15 + 1 = 16$                          ~\\

      \item[(c)]    $c_{n} = 2n - 1$                                          ~\\
                    $c_{1} = 2(1) - 1 = 2 - 1 = 1$                            ~\\
                    $c_{2} = 2(2) - 1 = 4 - 1 = 3$                            ~\\
                    $c_{3} = 2(3) - 1 = 6 - 1 = 5$                            ~\\
                    $c_{4} = 2(4) - 1 = 8 - 1 = 7$                            ~\\
                    $c_{5} = 2(5) - 1 = 10 - 1 = 9$                           ~\\

      \item[(d)]    $d_{n} = n^{2} + 4n + 4$                                  ~\\
                    $d_{1} = 1^{2} + 4(1) + 4 = 1 + 4 + 4 = 9$                ~\\
                    $d_{2} = 2^{2} + 4(2) + 4 = 4 + 8 + 4 = 16$               ~\\
                    $d_{3} = 3^{2} + 4(3) + 4 = 9 + 12 + 4 = 25$              ~\\
                    $d_{4} = 4^{2} + 4(4) + 4 = 16 + 8 + 4 = 28$              ~\\
                    $d_{5} = 5^{2} + 4(5) + 4 = 25 + 20 + 4 = 49$             ~\\
    \end{enumerate}

  \newpage
  \item   Finding values from recursive formulas
    \begin{enumerate}
      \item[(a)]    $a_{1} = 1$ \tab $a_{n} = a_{n-1} + 1$                    ~\\
                    $a_{2} = a_{1} + 1 = 1 + 1 = 2 $                          ~\\
                    $a_{3} = a_{2} + 1 = 2 + 1 = 3 $                          ~\\
                    $a_{4} = a_{3} + 1 = 3 + 1 = 4 $                          ~\\
                    $a_{5} = a_{4} + 1 = 4 + 1 = 5 $                          ~\\
                    
      \item[(b)]    $b_{1} = 2$ \tab $b_{n} = 2b_{n-1}$                       ~\\
                    $b_{2} = 2b_{1} = 2(2)  = 4$                              ~\\
                    $b_{3} = 2b_{2} = 2(4)  = 8$                              ~\\
                    $b_{4} = 2b_{3} = 2(8)  = 16$                             ~\\
                    $b_{5} = 2b_{4} = 2(16) = 32$                             ~\\
                    
      \item[(c)]    $c_{1} = 1$ \tab $c_{n} = 3c_{n-1} + 2$                   ~\\
                    $c_{2} = 3c_{1} + 2 = 3(1) + 2 = 5$                       ~\\
                    $c_{3} = 3c_{2} + 2 = 3(5) + 2 = 17$                      ~\\
                    $c_{4} = 3c_{3} + 2 = 3(17) + 2 = 53$                     ~\\
                    $c_{5} = 3c_{4} + 2 = 3(a) + 2 = 161$                     ~\\
                    
      \item[(d)]    $d_{1} = 1, d_{2} = 1$ \tab $d_{n} = d_{n-1} + d_{n-2}$   ~\\
                    $d_{3} = d_{2} + d_{1} = 1 + 1 = 2$                                                ~\\
                    $d_{4} = d_{3} + d_{2} = 2 + 1 = 3$                                                ~\\
                    $d_{5} = d_{4} + d_{3} = 3 + 2 = 5$                                                ~\\
                    $d_{6} = d_{5} + d_{4} = 5 + 3 = 8$                                                ~\\
                    
    \end{enumerate}

  \item   Evaluating sums
    \begin{enumerate}
      \item[(a)]    $ \sum_{k=1}^{4}(3k) $ ~\\
                    $k=1: \tab 3(1) = 3$ ~\\
                    $k=2: \tab 3(2) = 6$ ~\\
                    $k=3: \tab 3(3) = 9$ ~\\
                    $k=4: \tab 3(4) = 12$ ~\\
                    $= 3 + 6 + 9 + 12$ \tab[2cm] = 30
                    
                    
      \item[(b)]    $ \sum_{k=1}^{5}(4) $ ~\\
                    $k=1: \tab 4$ ~\\
                    $k=2: \tab 4$ ~\\
                    $k=3: \tab 4$ ~\\
                    $k=4: \tab 4$ ~\\
                    $k=5: \tab 4$ ~\\
                    $= 4 + 4 + 4 + 4 + 4 = $ \tab[2cm] = 20
    \end{enumerate}

  
  \item   \textbf{Recursive / Closed formula equivalence} \tab Show that the sequence defined by $a_{n} = a_{n-1} + 2; a_{1} = 5$ for $k \geq 2$ is equivalently described by the closed formula, $a_{n} = 2n+3$. ~\\
    \begin{enumerate}
      \item   \textbf{Step 1: Check $a_{1}$ for both formulas.}~\\
                Recursive: $a_{1} = 5$; \tab
                Closed: $a_{1} = 2(1) + 3 = 5$
                \tab \checkmark
      \item   \textbf{Step 2: Rewrite the recursive formula in terms of $m$:}~\\
                $a_{m} = 4 \cdot a_{m-1} + 2$
      \item   \textbf{Step 3: Find the equation for $a_{m-1}$ via the closed formula:}~\\
                $$ a_{m-1} = 2(m-1) + 3 \tab{}
                = 2m - 2 + 3 \tab{}
                = 2m + 1
                $$
      \item   \textbf{Step 4: Plug $a_{m-1}$ back into the recursive formula and simplify.}~\\
                $ a_{m} = a_{m-1} + 2 $ ~\\
                $ a_{m} = (2m+1) + 2 $ ~\\
                $ a_{m} = 2m + 3 $
    \end{enumerate}

  \item   \textbf{Recursive / Closed formula equivalence} \tab Show that the sequence defined by $ a_{k} = 2 \cdot a_{k-1} + 1 ; a_{1} = 1 $ for $k \geq 2$ is equivalently described by the closed formula, $ a_{n} = 2^{n} - 1 $
    \begin{enumerate}
      \item   \textbf{Step 1: Check $a_{1}$ for both formulas.} ~\\
                Recursive:
                $ a_{1} = 1 $
                \tab
                Closed:
                $ a_{1} = 2^{1} - 1  = 1$
      \item   \textbf{Step 2: Rewrite the recursive formula in terms of $m$:} ~\\
                $ a_{m} = 2 \cdot a_{m-1} + 1 $
      \item   \textbf{Step 3: Find the equation for $a_{m-1}$ via the closed formula:} ~\\
                $$ a_{m-1} = 2^{m-1} - 1
                \tab{}
                = 2^{m} \cdot 2^{-1} - 1
                \tab{}
                = \frac{2^{m}}{2^{1}} - 1
                $$
      \item   \textbf{Step 4: Plug $a_{m-1}$ back into the recursive formula and simplify.} ~\\
                $ a_{m} = 2 \cdot a_{m-1} + 1 $ ~\\
                $ a_{m} = 2^{1}(\frac{2^{m}}{2^{1}} - 1) + 1 $ ~\\
                $ a_{m} = 2^{m} - 2 + 1 $ ~\\
                $ a_{m} = 2^{m} - 1 $
    \end{enumerate}
    
  \newpage
  \item   \textbf{Summation / Closed formula equivalence} \tab  Use induction to prove $ \sum_{i=1}^{n} (2i+4) = n^{2} + 5n$ for each $n \geq 1$.
    \begin{enumerate}
      \item   \textbf{Step 1: Show that the proposition is true for 1, 2, and 3. } ~\\
                \begin{tabular}{l | l | l }
                    \textbf{ $i$ value } &
                    \textbf{ $ \sum_{i=1}^{n} (2i+4) $ } &
                    \textbf{ $n^{2} + 5n$ }
                    \\ \hline
                    $i = 1$ & \solution{ $2(1)+4 = 6$ }{} & \solution{ $1^{2} + 5(1) = 6$ }{}  \\
                    $i = 2$ & \solution{ $6 + 2(2)+4 = 14$ }{} & \solution{ $2^{2} + 5(2) = 14$ }{}  \\
                    $i = 3$ & \solution{ $14 + 2(3) + 4 = 24$ }{} & \solution{ $3^{2} + 5(3) = 9+15 = 24$ }{} 
                \end{tabular}
      \item   \textbf{Step 2: Rewrite the summation} as the sum from $i = 1$ to $m-1$, plus the final term $(2m+4)$. ~\\
                $ \sum_{i=1}^{m} (2i+4) = \sum_{i=1}^{m-1} (2i+4) + (2m+4)$
      \item   \textbf{Step 3: Find an equation} for $\sum_{i=1}^{m-1}$ via the proposition: ~\\
                $ \sum_{i=1}^{m-1} (2i+4) = (m-1)^{2} + 5(m-1) $ ~\\
                $ \sum_{i=1}^{m-1} (2i+4) = m^{2} - 2m + 1 + 5m - 5 $ ~\\
                $ \sum_{i=1}^{m-1} (2i+4) = m^{2} + 3m - 4 $
      \item   \textbf{Step 4: Plug} $\sum_{i=1}^{m-1}$ into the summation from step 2 and simplify: ~\\
                $ \sum_{i=1}^{m} (2i+4) = (m^{2} + 3m - 4) + (2m+4) $ ~\\
                $ \sum_{i=1}^{m} (2i+4) = m^{2} + 5m $ ~\\
                This matches the original proposition.
    \end{enumerate}
    
  \item   \textbf{Summation / Closed formula equivalence} \tab 
    \begin{enumerate}
      \item   \textbf{Step 1: Show that the proposition is true for 1, 2, and 3. } ~\\
                \begin{tabular}{l | l | l }
                    \textbf{ $i$ value } & \textbf{ $ \sum_{i=1}^{n} (2i+4) $ } & \textbf{ $n^{2} + 5n$ } \\ \hline
                    $i = 1$ & \solution{ $1$ }{} & \solution{ $\frac{1(2)}{2} = 1$ }{}  \\
                    $i = 2$ & \solution{ $1 + 2 = 3$ }{} & \solution{ $ \frac{2(2+1)}{2} = 3 $ }{}  \\ 
                    $i = 3$ & \solution{ $ 1 + 2 + 3 = 6$ }{} & \solution{ $ \frac{3(3+1)}{2} = 6 $ }{} 
                \end{tabular}
      \item   \textbf{Step 2: Rewrite the summation} as the sum from $i = 1$ to $m-1$, plus the final term $m$. ~\\
                $ \sum_{i=1}^{m} i = \sum_{i=1}^{m-1} (i) + m$
      \item   \textbf{Step 3: Find an equation} for $\sum_{i=1}^{m-1}$ via the proposition: ~\\
                $ \sum_{i=1}^{m-1} i = \frac{(m-1)(m)}{2} $ ~\\
                $ \sum_{i=1}^{m-1} i = \frac{m^{2} - m}{2} $
      \item   \textbf{Step 4: Plug} $\sum_{i=1}^{m-1}$ into the summation from step 2 and simplify: ~\\
                $ \sum_{i=1}^{m} i = \frac{m^{2} - m}{2} + m $ ~\\
                $ \sum_{i=1}^{m} i = \frac{m^{2} - m}{2} + \frac{2m}{2} $ ~\\
                $ \sum_{i=1}^{m} i = \frac{m^{2} + m}{2} $ ~\\
                $ \sum_{i=1}^{m} i = \frac{m(m + 1)}{2} $ ~\\
                This matches the original proposition.
    \end{enumerate}
    
  \newpage
  \item   \textbf{Sums as Recursive sequences:} \tab Consider the sum $\sum_{i=1}^{n} (3i^{2})$. Use the notation $s_{n}$ to denote this sum. Find a recursive description of $s_{n}$.
    \begin{enumerate}
      \item   \textbf{Step 1: Find $s_{1}$} ~\\ 
              $s_{1} = \sum_{i=1}^{1} (3i^{2}) = 3(1)^{2} = 3$
      \item   \textbf{Step 2: Restate the result of $s_{n}$ as $s_{n-1}$ plus the final term:} ~\\
              $s_{n} = s_{n-1} + 3n^{2}$
      \item   \textbf{The full recursive formula:} ~\\
              $s_{1} = 3$ 
              \tab[4cm]
              $s_{n} = s_{n-1} + 3n^{2}$
    \end{enumerate}
    
  \item \textbf{Sums as Recursive sequences:} \tab Consider the sum $\sum_{i=1}^{n} (2^{i-1}+1)$. Use the notation $s_{n}$ to denote this sum. Find a recursive description of $s_{n}$.
    \begin{enumerate}
      \item   \textbf{Step 1: Find $s_{1}$} ~\\
              $s_{1} = \sum_{i=1}^{1} (2^{i-1}+1) = 2^{0} + 1 = 2$
      \item   \textbf{Step 2: Rewrite $s_{n}$ in terms of $s_{n-1}$ plus final term} ~\\
              $s_{n} = s_{n-1} + 2^{n-1} + 1$
      \item   \textbf{The full recursive formula:} ~\\
              $s_{1} = 2$ 
              \tab[4cm]
              $s_{n} = s_{n-1} + 2^{n-1} + 1$
    \end{enumerate}
\end{enumerate}
}{}
\input{BASE-FOOT}


