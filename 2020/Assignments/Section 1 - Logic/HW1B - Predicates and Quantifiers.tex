\input{../BASE-1-HEAD}
\newcommand{\laClass}       {Discrete Structures I}
\newcommand{\laTitle}       {HW1B Logic - Predicates and Quantifiers}
\newcommand{\laTextbookA}   {Ensley \& Crawley: Chapter 1.4}
\newcommand{\laTextbookB}   {Johnsonbaugh: Chapter 1.5, 1.6}
\newcommand{\laTextbookC}   {ZyBooks: Chapter 1.4}
\newcounter{question}

\toggletrue{answerkey}      \togglefalse{answerkey}

\input{../BASE-2-HEADER}
\input{../BASE-3-INSTRUCTIONS-HOMEWORK}
% ASSIGNMENT ------------------------------------ %

    \section*{\laTitle}

    \notonkey{
    \subsection*{Predicates}

    \begin{intro}{Predicates\\}
        \large
        In mathematical logic, a \textbf{predicate} is commonly understood to be a Boolean-valued function.
        \footnote{From https://en.wikipedia.org/wiki/Predicate\_(mathematical\_logic)}

        We write a predicate as a function, such as $P(x)$, for example:

        \begin{center}
            $P(x)$ is the predicate, ``x is less than 2".
        \end{center}

        Once some value is plugged in for $x$, the result is a proposition -
        something either unambiguously \textbf{true} or \textbf{false},
        but until we have some input for $x$, we don't know whether it
        is true or false.

        \begin{center}
            $P(0)$ = true \tab $P(2)$ = false \tab $P(10)$ = false
        \end{center}

        Additionally, predicates can also be combined with the logical
        operators AND $\land$ OR $\lor$ and NOT $\neg$.
        \normalsize
    \end{intro}


    \newpage
    }{}
    
    
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        For the following predicates given, plug in \textbf{2, 23, -5, and 15}
        as inputs and write out whether the result is true or false.
        
        \begin{enumerate}
            \item[a.]   $P(x)$ is the predicate ``$x > 15$"
                        \begin{itemize}
                            \item   $P(2) = $       \solution{False}{}
                            \item   $P(23) = $      \solution{True}{}
                            \item   $P(-5) = $      \solution{False}{}
                            \item   $P(15) = $      \solution{False}{}
                        \end{itemize}
            \item[b.]   $Q(x)$ is the predicate ``$x \leq 15$"
                        \begin{itemize}
                            \item   $P(2) = $       \solution{True}{}
                            \item   $P(23) = $      \solution{False}{}
                            \item   $P(-5) = $      \solution{True}{}
                            \item   $P(15) = $      \solution{True}{}
                        \end{itemize}
            \item[c.]   $R(x)$ is the predicate ``$(x > 5) \land (x < 20)$"
                        \begin{itemize}
                            \item   $P(2) =  $  \solution{False}{}
                            \item   $P(23) = $  \solution{False}{}
                            \item   $P(-5) = $  \solution{False}{}
                            \item   $P(15) = $  \solution{True}{}
                        \end{itemize}
        \end{enumerate}
    \end{questionNOGRADE}
    
    \newpage

    \notonkey{
    \begin{intro}{Domain\\}
        \large
        When we're working with predicates, we will also define the domain.
        The \textbf{domain} is the set of all possible inputs for our predicate.
        In other words, $x$ must be chosen from the domain.
        \normalsize
    \end{intro}
    }{}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        For the following predicates and domains given, specify
        whether the predicate is true for \textbf{all members of the domain},
        \textbf{some members of the domain}, or \textbf{no members of the domain.}
        
        \begin{enumerate}
            \item[a.]   $P(x)$ is the predicate ``$x > 15$", the domain is \{10, 12, 14, 16, 18\}.
                        ~\\
                        \Square \ True for all \tab \Square \ True for some \tab \Square \ True for none \\
                        \solution{ True for some }{}
            
            \item[b.] $Q(x)$ is the predicate ``$x \leq 15$", the domain is \{0, 1, 2, 3\}.
                        ~\\
                        \Square \ True for all \tab \Square \ True for some \tab \Square \ True for none \\
                        \solution{ True for all }{}
                        
            \item[c.] $R(x)$ is the predicate ``$(x > 5) \land (x < 20)$", the domain is \{0, 1, 2\}.
                        ~\\
                        \Square \ True for all \tab \Square \ True for some \tab \Square \ True for none \\
                        \solution{ True for none }{}
                        
            \item[d.] $S(x)$ is the predicate ``$(x > 1) \land (x < 5)$", domain is \{2, 3, 4\}.
                        ~\\
                        \Square \ True for all \tab \Square \ True for some \tab \Square \ True for none \\
                        \solution{ True for all }{}
        \end{enumerate}
    \end{questionNOGRADE}
    
    \notonkey{
    
    \newpage
    \paragraph{Question 1 solutions} ~\\
    
    \small
    \begin{tabular}{l l l}
		a. & b. \\
		$P(x)$ is the predicate ``$x > 15$" 					& $Q(x)$ is the predicate ``$x \leq 15$" \\
		$P(2) = $  \answer{ $2 > 15$; false } \color{black}		& $P(2) = $  \answer{ $2  \leq 15$; true } \color{black}
		\\
		$P(23) = $ \answer{ $23 > 15$; true } \color{black}		& $P(23) = $ \answer{ $23 \leq 15$; false } \color{black}
		\\
		$P(-5) = $ \answer{ $-5 > 15$; false } \color{black}	& $P(-5) = $ \answer{ $-5 \leq 15$; true } \color{black}
		\\
		$P(15) = $ \answer{ $15 > 15$; false } \color{black}	& $P(15) = $ \answer{ $15 \leq 15$; true } \color{black}
		
		\\
		c.
		\\
		$R(x)$ is the predicate ``$(x > 5) \land (x < 20)$" \\
		$P(2) = $	\answer{ $(2 > 5)  \land (2 < 20)$; false } \color{black} \\
		$P(23) = $	\answer{ $(23 > 5) \land (23 < 20)$; false } \color{black} \\
		$P(-5) = $	\answer{ $(-5 > 5) \land (-5 < 20)$; false } \color{black} \\
		$P(15) = $	\answer{ $(15 > 5) \land (15 < 20)$; true } \color{black} \\
    \end{tabular}
    \normalsize

	\paragraph{Question 2 solutions} ~\\
	
        \begin{enumerate}
            \item[a.]   $P(x)$ is the predicate ``$x > 15$", the domain is \{10, 12, 14, 16, 18\}.
                        ~\\
                        \color{red}
                        True for \textit{some}; 10 is not greater than 15.
                        \color{black}
            
            \item[b.] $Q(x)$ is the predicate ``$x \leq 15$", the domain is \{0, 1, 2, 3\}.
                        ~\\
                        \color{red}
                        True for \textit{all}; all values 0, 1, 2, and 3 are less than or equal to 15.
                        \color{black}
                        
            \item[c.] $R(x)$ is the predicate ``$(x > 5) \land (x < 20)$", the domain is \{0, 1, 2\}.
                        ~\\
                        \color{red}
                        True for \textit{none}; no values are greater than 5.
                        \color{black}
                        
            \item[d.] $S(x)$ is the predicate ``$(x > 1) \land (x < 5)$", domain is \{2, 3, 4\}.
                        ~\\
                        \color{red}
                        True for \textit{all}; all are greater than 1 and less than 5.
                        \color{black}
        \end{enumerate}
    
	\newpage

    \subsection*{Quantifiers}

    \begin{intro}{Quantifiers\\}
        \large
        Symbolically, we can specify that the input of our predicate, $x$,
        belongs in some domain set $D$ with the notation: $x \in D$.
        This is read as, ``$x$ exists in the domain $D$."

        ~\\
        Additionally, we can also specify whether a predicate is true
        \textbf{for all inputs $x$ from the domain $D$} using the ``for all"
        symbol $\forall$, or we can specify that the predicate is true
        \textbf{for \textit{some} inputs $x$ from the domain $D$} using
        the ``there exists" symbol $\exists$.{}

        \paragraph{Example:} Rewrite the predicate symbolically. ~\\
            $P(x)$ is ``$x > 15$", the domain D is \{16, 17, 18\}.
            Here we can see that all inputs from the domain will
            result in the predicate evaluating to true, so we can write:

            \begin{center}
                $ \forall x \in D, P(x) $ (``For all x in D, x is greater than 15.")
            \end{center}

        \begin{itemize}
            \item The symbol $\in$ (``in") indicates membership in a set.
            \item The symbol $\forall$ (``for all") means "for all", or "every".
            \item The symbol $\exists$ (``there exists") means "there is (at least one)", or "there exists (at least one)".
            \item The symbols $\forall$ and $\exists$ are called \textbf{quantifiers}. When used
                with predicates, the statement is called a \textbf{quantified predicate}.
        \end{itemize}
        \normalsize
    \end{intro}

    \newpage
    
    }{}
    
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        For the following predicates, rewrite the sentence symbolically,
        as in the example above.
        Use either $\forall$ or $\exists$, based on whether the
        predicate is true for the domain given.
        
        \paragraph{Hint:}
            If a predicate P(x) is false for all elements in the domain, you
            can phrase it as: ``$\forall x \in D, \neg P(x)$".
        
        \begin{enumerate}
            \item[a.]   $P(x)$ is the predicate ``$x > 15$", the domain is \{10, 12, 14, 16, 18\}.
                        \solution{ \\ $\exists x \in D, P(x)$ }{ \vspace{2cm} }
            \item[b.]   $Q(x)$ is the predicate ``$x \leq 15$", the domain is \{0, 1, 2, 3\}.
                        \solution{ \\ $\forall x \in D, Q(x)$ }{ \vspace{2cm} }
            \item[c.]   $R(x)$ is the predicate ``$(x > 5) \land (x < 20)$", the domain is \{0, 1, 2\}.
                        \solution{ \\ $\forall x \in D, \neg R(x)$ }{ \vspace{2cm} }
            \item[d.]   $S(x)$ is the predicate ``$(x > 1) \land (x < 5)$", domain is \{2, 3, 4\}.
                        \solution{ \\ $\forall x \in D, S(x)$ }{ \vspace{2cm} }
        \end{enumerate}
    \end{questionNOGRADE}
    
    \notonkey{
    \newpage
    
    \paragraph{Question 3 solutions} ~\\
    
	\begin{enumerate}
		\item[a.]   $P(x)$ is the predicate ``$x > 15$", the domain is \{10, 12, 14, 16, 18\}. ~\\
					\color{red}
					$\exists x \in D, P(x)$
					\color{black}
					
		\item[b.]   $Q(x)$ is the predicate ``$x \leq 15$", the domain is \{0, 1, 2, 3\}. ~\\
					\color{red}
					$\forall x \in D, Q(x)$
					\color{black}
					
		\item[c.]   $R(x)$ is the predicate ``$(x > 5) \land (x < 20)$", the domain is \{0, 1, 2\}. ~\\
					\color{red}
					$\forall x \in D, \neg R(x)$
					
					You can expand it like this: For all elements in the domain... ~\\
					
					$\neg R(x)$ ~\\
					$\equiv \neg[ (x > 5) \land (x < 20) ]$ ~\\
					$\equiv \neg(x > 5) \lor \neg (x < 20)$ ~\\
					$\equiv (x \leq 5) \lor (x \geq 20)$ ~\\
					
					Then, you can check this logic with the elements of the set...
					
					\begin{itemize}
						\item	$(0 \leq 5) \lor (0 \geq 20)$? TRUE; 0 is less than 5.
						\item	$(1 \leq 5) \lor (1 \geq 20)$? TRUE; 1 is less than 5.
						\item	$(2 \leq 5) \lor (2 \geq 20)$? TRUE; 2 is less than 5.
					\end{itemize}
					\color{black}
					
		\item[d.]   $S(x)$ is the predicate ``$(x > 1) \land (x < 5)$", domain is \{2, 3, 4\}. ~\\
					\color{red}
					$\forall x \in D, S(x)$
					\color{black}
					
	\end{enumerate}

    \newpage
    }{}
    
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        For the following predicates, rewrite the sentence symbolically
        using the domain D = \{ 3, 4, 5, 10, 20, 25 \}.
        Make sure to define your predicates (state that ``$P(x)$ is the predicate...",
        and afterwards specify whether the quantified predicate is true or false.
        (If it states ``there exists" but none exist, then the quantified predicate is false.)
        
        \begin{enumerate}
            \item[a.]   There exists some $m$ that is a member of $D$, such that $m \geq 3$.
                        \solution{ \\
                            $P(m)$ is the predicate, $m \geq 3$ \\
                            $\exists m \in D, P(m)$ 
                        }{ \vspace{3cm} }
                        
            \item[b.]   There exists some element in $D$ that is negative.
                        \solution{ 
                        \\ 
                        $Q(x)$ is the predicate, $x$ is negative. (or $x < 0$.) \\
                        $\exists x \in D, Q(x)$
                        }{ \vspace{3cm} }
                        
            \item[c.]   There is (at least one) $k$ in the set $D$ with the property that $k^{2}$ is also in the set $D$.

                        \solution{ 
                        $R(k)$ is the predicate, $k^2 \in D$. \\
                        $\exists k \in D, R(k)$
                        }{
                        \begin{hint}{Hint}
                            How can you specify the predicate, ``$k^{2}$ is in the set $D$" symbolically?
                        \end{hint}
                        \vspace{3cm} }
        \end{enumerate}
    \end{questionNOGRADE}

    \notonkey{
    \newpage
    \subsection*{Negating quantifiers}

        \begin{intro}{Negating quantifiers}
            \large            
            For any predicates $P$ and $Q$ over a domain $D$,
            \begin{itemize}
                \item The negation of $\forall x \in D, P(x)$ is $\exists x \in D, \neg P(x)$.
                \item The negation of $\exists x \in D, P(x)$ is $\forall x \in D, \neg P(x)$.
            \end{itemize}

            When negating a predicate that uses an equal sign, the negation would be ``not equals".

            \paragraph{Example 1:} Negate $\forall x \in \mathbb{Z}, x > 0$.
            \small ~\\ \textit{(For all integers $x$, $x$ is greater than 0.)} \large
            \begin{enumerate}
                \item   $\neg( \forall x \in \mathbb{Z}, x > 0 )$  
                \item   $\equiv \neg( \forall x \in \mathbb{Z} )$, $\neg( x > 0 ) $
                \item   $\equiv \exists x \in \mathbb{Z}, x \leq 0$
                        ~\\ \footnotesize \textit{(There exists some integer $x$ such that $x$ is less than or equal to than 0.)} \large
            \end{enumerate}

            In this case, the original statement ``$\forall x \in \mathbb{Z}, x > 0$'' was incorrect, and the negation ``$\exists x \in \mathbb{Z}, x \leq 0$'' is valid.
        \end{intro}
        
    }{}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Write the negation of each of these statements. Simplify as much as possible.
        Identify whether the original statement or the negation is true.
        
        \begin{enumerate}
            \item[a.]   $\forall x \in \mathbb{Z},$ \tab[0.2cm]  \tab[0.2cm] $P(x)$.    ~\\ $P(x)$ is ``$2x$ is even''.
                        \solution{ \\
                        $\exists x \in \mathbb{Z}, \neg P(x)$ \\
                        There exists some integer $x$ such that $2x$ is NOT even. \\
                        Original is true; 2 times any integer is always even.
                        }{ \vspace{2cm} }
                        
            \item[b.]   $\exists x \in \mathbb{N},$ \tab[0.2cm]  \tab[0.2cm] $Q(x)$.    ~\\ $Q(x)$ is ``$x \cdot x < 0$''.
                        \solution{ \\
                        $\forall x \in \mathbb{N}, \neg Q(x)$ \\
                        For all natural numbers $x$, $x \cdot x \geq 0$. \\
                        Negation is true; there are no natural numbers less than 0.
                        }{ \vspace{2cm} }
                        
            \item[c.]   $\forall x \in \mathbb{N},$ \tab[0.2cm]  \tab[0.2cm] $R(x)$.    ~\\ $R(x)$ is ``$x \in \mathbb{Z}$''.
                        \solution{ \\
                        $\exists x \in \mathbb{N}, \neg R(x)$ \\
                        There exists some natural number $x$ such that $x$ is not an integer. \\
                        The original is true; any natural number is also an integer (but not vice-versa).
                        }{ \vspace{2cm} }
        \end{enumerate}
    \end{questionNOGRADE}

    \notonkey
    {
	\newpage
	
	\paragraph{Question 4 solution} ~\\

	\begin{enumerate}
		\item[a.]   There exists some $m$ that is a member of $D$, such that $m \geq 3$. ~\\
					\color{red}
					$ P(m) $ is the predicate, $ m \geq 3$. ~\\
					$ \exists m \in D, P(m) $
					\color{black}
					
		\item[b.]   There exists some element in $D$ that is negative.~\\
					\color{red}
					$ Q(m) $ is the predicate, $m < 0$. ~\\
					$ \exists m \in D, Q(m)$
					\color{black}
					
		\item[c.]   There is (at least one) $k$ in the set $D$ with the property that $k^{2}$ is also in the set $D$.~\\
					\color{red}
					$ R(k) $ is the predicate, $k^2 \in D$. ~\\
					$ \exists k \in D, R(k) $
					\color{black}
	\end{enumerate}
	
	\paragraph{Question 5 solution} ~\\

	\begin{enumerate}
		\item[a.]   $\forall x \in \mathbb{Z},$ \tab[0.2cm]  \tab[0.2cm] $P(x)$.    $P(x)$ is ``$2x$ is even''. ~\\
					\color{red}
					$\neg [ \forall x \in \mathbb{Z}, P(x) ]$ ~\\
					$\equiv \exists x \in \mathbb{Z}, \neg P(x) $ ~\\
					$\equiv \exists x \in \mathbb{Z}, 2x$ is not even.
					\color{black}
					
		\item[b.]   $\exists x \in \mathbb{N},$ \tab[0.2cm]  \tab[0.2cm] $Q(x)$.    $Q(x)$ is ``$x \cdot x < 0$''.~\\
					\color{red}
					$\neg[ \exists x \in \mathbb{N}, Q(x) ]$ ~\\
					$\equiv \forall x \in \mathbb{N}, \neg Q(x)$ ~\\
					$\equiv \forall x \in \mathbb{N}, x \cdot x \geq 0$ ~\\
					\color{black}
					
		\item[c.]   $\forall x \in \mathbb{N},$ \tab[0.2cm]  \tab[0.2cm] $R(x)$.    $R(x)$ is ``$x \in \mathbb{Z}$''.~\\
					\color{red}
					$\neg [\forall x \in \mathbb{N}, R(x)] $ ~\\
					$\exists x \in \mathbb{N}, \neg R(x) $ ~\\
					$\exists x \in \mathbb{N}, x \not\in \mathbb{Z} $ ~\\
					\color{black}
					
	\end{enumerate}
	
    \newpage
    }{}
    
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Which elements of the set $D$ = \{2, 4, 6, 8, 10, 12\} make the
        \textbf{negation} of each of these predicates true? List the numbers from $D$ that make the \underline{negation} true.
        
        \begin{enumerate}
            \item[a.]   $Q(n)$ is the predicate, ``$n > 10$".
                        \solution{
                        Negation is $n \leq 10$, so 2, 4, 6, 8, 10.
                        }{ \vspace{3cm} }
                        
            \item[b.]   $R(n)$ is the predicate, ``$n$ is even".
                        \solution{
                        Negation is $n$ is odd, so nothing.
                        }{ \vspace{3cm} }
                        
            \item[c.]   $S(n)$ is the predicate, ``$n^{2} < 1$".
                        \solution{
                        Negation is $n^2 \geq 1$, so all numbers.
                        }{ \vspace{3cm} }
                        
            \item[d.]   $T(n)$ is the predicate, ``$n-2$ is an element of $D$".
                        \solution{
                        Negation is $n-2$ is NOT an element of $D$, so 2.
                        }{ \vspace{3cm} }
        \end{enumerate}
    \end{questionNOGRADE}
    
    
    \notonkey{
	\newpage

    \section*{Overview questions}
    
    }
    
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
		$P(x)$ is the proposition, ``$x$ times 3 is even.'' State whether each of the following is $true$ or $false$. The domain is $\mathbb{Z}$ - the set of integers.
		\begin{itemize}
			\item[a.]   $P(1)$  \solution{ $1 \cdot 3 = 3$, not even - false }{ \vspace{1cm} }
			\item[b.]   $P(2)$  \solution{ $2 \cdot 3 = 6$, even - true }{ \vspace{1cm} }
			\item[c.]   $P(5)$  \solution{ $5 \cdot 3 = 15$, not even - false }{ \vspace{1cm} }
			\item[d.]   $P(10)$ \solution{ $10 \cdot 3 = 30$, even - true }{ \vspace{1cm} }
			\item[e.]   $P(2x)$ \solution{ $2x \cdot 3 = 6x$, anything multipliesd by 2 will be even - true }{ \vspace{1cm} }
		\end{itemize}
    \end{questionNOGRADE}
    
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
		For each of the following, identify whether the statements are true for \textbf{all}, \textbf{some}, or \textbf{no} elements of the domain given.
		\begin{itemize}
			\item[a.]   $O(n)$ is the predicate, ``$n$ is an odd number''. The domain is $\mathbb{Z}$.
                        \solution{ \\ True for some }{ \vspace{1cm} }
			\item[b.]   $N(n)$ is the predicate, ``$n$ is a positive number''. The domain is $\mathbb{N}$.
                        \solution{ \\ True for some; 0 is included in $\mathbb{N}$. }{ \vspace{1cm} }
			\item[c.]   $S(n)$ is the predicate, ``$n$ plus $n+1$ results in an odd number. The domain is $\mathbb{Z}$.
                        \solution{ \\ 2 + 3 = 5, 3 + 4 = 7, 4 + 5 = 9, etc... \\
                        We will learn to prove later, but true for all.
                         }{ \vspace{1cm} }
			\item[d.]   $E(n)$ is the predicate, ``2 times $n$ plus 1 is an even number''. The domain is $\mathbb{Z}$.
                        \solution{
                        True for none; $2n + 1$ is always an odd number.
                        }{ \vspace{1cm} }
		\end{itemize}
    \end{questionNOGRADE}
    
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
		Translate the following into a symbolic statement. Define your predicate function and domain.
		\begin{itemize}
			\item[a.]   ``All college students are tired.''
                        \solution{ \\
                            $C$ = domain of all college students, $T(c)$ is the predicate, ``$c$ is tired.'' \\
                            $\forall c \in C, T(c)$
                        }{ \vspace{1cm} }
			\item[b.]   ``There is at least one college student who does not like pizza.''
                        \solution{ \\
                            $C$ = domain of all college students, $P(c)$ is the predicate, ``$c$ likes pizza.'' \\
                            $\exists c \in C, \neg P(c)$
                        }{ \vspace{1cm} }

			\item[d.]   ``There is some college student who studied and did not pass their class.''
                        \solution{ \\
                            $C$ = domain of all college students, $S(c)$ is the predicate, ``$c$ studied.'' \\ $P(c)$ is the predicate,  ``$c$ passed their class.'' \\
                            $\exists c \in C, S(c) \land \neg P(c)$
                        }{ \vspace{1cm} }
		\end{itemize}
    \end{questionNOGRADE}
    
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
		Give some discrete (finite) domain that makes the following statement true for \textbf{all} elements of the domain.
		\begin{itemize}
			\item[a.]   $A(x)$ is the predicate, ``$x > 100$''      \solution{ \\ Many solutions; example: 101, 102, 103 }{ \vspace{1cm} }
			\item[b.]   $B(x)$ is the predicate, ``$-2x > x$''      \solution{ \\ Many solutions; example: -2, -3, -4 }{ \vspace{1cm} }
			\item[c.]   $C(x)$ is the predicate, ``$\sqrt{x} \in \mathbb{Z}$''
                        \solution{ \\ Many solutions; example: 4, 16, 144 }{ \vspace{1cm} }
		\end{itemize}
    \end{questionNOGRADE}
    
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
		For each of the following statements, write the negation both symbolically and in English, and identify whether the \textbf{original is true} or the \textbf{negation is true}.
		\begin{itemize}
			\item[a.]   $\forall x \in D, E(x) \land P(x)$ ~\\
						Where $D = \{ 2, 4, 6, 8 \}$, $E(x)$ is the predicate, ``$x$ is even'', and $P(x)$ is the predicate, ``$x > 0$''.
                        \solution{ \\
                        $\neg ( \forall x \in D, E(x) \land P(x) )$ \\
                        $ = \exists x \in D, \neg( E(x) \land P(x) )$ \\
                        $ = \exists x \in D, \neg E(x) \lor \neg P(x)$  \\
                        There exists some element $x$ in the domain $D$,
                        such that $x$ is odd OR $x \leq 0$. \\
                        No elements are either odd OR less than 0, so the original is true.
                        }{ \vspace{2cm} }
                        
			\item[b.]   $\forall x \in E, O(x) \land P(x)$ ~\\
						Where $E = \{ 1, 2, 3, 4 \}$, $O(x)$ is the predicate, ``$x$ is odd'', and $P(x)$ is the predicate, ``$x > 0$''.
                        \solution{ \\
                        $\neg ( \forall x \in E, O(x) \land P(x) )$ \\
                        $ \exists x \in E, \neg( O(x) \land P(x) )$ \\
                        $ \exists x \in E, \neg O(x) \lor \neg P(x) $ \\
                        There exists some element in E, such that $x$ is even, OR $x \leq 0$. \\
                        The negation is true; there are some elements that are even. \\
                        The original is false; it states that all elements are odd AND greater than 0.
                        }{ \vspace{2cm} }
                        
		\end{itemize}
    \end{questionNOGRADE}
    
    
\input{../BASE-4-FOOT}
