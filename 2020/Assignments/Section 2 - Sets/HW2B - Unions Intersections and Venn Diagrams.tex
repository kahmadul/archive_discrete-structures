\input{../BASE-1-HEAD}
\newcommand{\laClass}       {Discrete Structures I}
\newcommand{\laTitle}       {HW2B Sets - $\cup$, $\cap$, and Venn Diagrams}
\newcommand{\laTextbookA}   {Ensley \& Crawley: Chapter 3.1}
\newcommand{\laTextbookB}   {}
\newcommand{\laTextbookC}   {ZyBooks: Chapter 2.3, 2.4}
\newcounter{question}
\renewcommand{\chaptername}{Part}

\toggletrue{answerkey}     	\togglefalse{answerkey}

\input{../BASE-2-HEADER}

\notonkey{

\chapter{Overview - Unions, intersections, and differences}

	%---%
	\section{Operation basics}
	We can build new sets from two existing sets by combining all their elements (\textbf{unions}),
	taking only their common elements (\textbf{intersection}), or taking one set but without
	any of the elements from the other (\textbf{difference}). ~\\
	
	Let's take two sets:
	
	\begin{center}
		$A = \{ 1, 2, 3 \}$ \tab $B = \{ 2, 3, 4 \}$
	\end{center}
	
	The \textbf{intersection} of $A$ and $B$, written symbolically as $A \cap B$, is:
	
	$$ A \cap B = \{ 2, 3 \} $$
	
	The \textbf{union} of $A$ and $B$, written symbolically as $A \cup B$, is:
	
	$$ A \cup B = \{ 1, 2, 3, 4 \}$$
	
	The \textbf{difference} of $A$ and $B$, written symbolically as $A - B$, is:
	
	$$ A - B = \{ 1 \}$$
	
	The \textbf{difference} of $B$ and $A$, written symbolically as $B - A$, is:
	
	$$ B - A = \{ 4 \}$$
	
	If you take the difference of two sets with \textit{no} elements in common,
	then the result is an empty set, $\emptyset$.
	
	\section{Complement of a set}
	Remember that a \textbf{universal set} $U$. When no universal set is explicitly
	mentioned, its elements will be the elements of all sets in the problem \\ ($A \cup B \cup C$...).
	Otherwise, the universal set can be defined explicitly to something, such as $U = \mathbb{Z}$. ~\\
	
	The \textbf{complement} of a some set $A$ (written $A'$) is the universal set $U$, with the elements
	of $A$ removed. In other words, $A' = U - A$.
	
	~\\
	Given \tab $U = \{ 2, 4, 6, 8, 10 \}$  and  $A = \{ 2, 4 \}$, \tab $A' = \{ 6, 8, 10 \}$.
	
	\section{Properties of sets}
	
		\begin{tabular}{l l}
			Commutative properties 			& $A \cap B = B \cap A$
			\\								& $A \cup B = B \cup A$ 
			\\ \\
			Associative properties			& $(A \cup B) \cup C = A \cup (B \cup C)$
			\\								& $(A \cap B) \cap C) = A \cap (B \cap C)$
			\\ \\
			Distributive properties			& $A \cup(B \cap C) = (A \cup B) \cap (A \cup C)$
			\\								& $A \cap (B \cup C) = (A \cap B) \cup (A \cap C)$
		\end{tabular}
			
		
\chapter{Overview - Venn diagrams}

	Venn diagrams can be used to visually represent sets and relationships between sets.
	A box is drawn to represent the universal set $U$, and then circles for each set are
	added. The circles overlap, and we can shade regions to indicate what parts of sets
	$A$, $B$, and $C$ are used. ~\\
	
	\def\circleA{(1.5,1.5) circle (1.0cm)}
	\def\circleB{(2.5,1.5) circle (1.0cm)}
	
	\begin{tabular}{c c c}
		%A%
		\begin{tikzpicture}
			\draw[fill=white] (0,0) -- (3,0) -- (3,3) -- (0,3) -- (0,0) node[anchor=south west] {$U$};
			\draw[fill=orange] (1.5, 1.5) circle (1cm) node[anchor=south east] {$A$};
		\end{tikzpicture}
		&
		
		%A'%
		\begin{tikzpicture}
			\draw[fill=orange] (0,0) -- (3,0) -- (3,3) -- (0,3) -- (0,0) node[anchor=south west] {$U$};
			\draw[fill=white] (1.5, 1.5) circle (1cm) node[anchor=south east] {$A$};
		\end{tikzpicture}
		\\
		
		Diagram of $A$
		&
		Diagram of $A'$
		\\
		\\
	
		% A intersection B
		 \begin{tikzpicture}
			 \draw[fill=white] (0,0) -- (4,0) -- (4,3) -- (0,3) -- (0,0) node[anchor=south west] {U};
			 \begin{scope}
				 \clip \circleA;
				 \fill[fill=orange] \circleB;
			 \end{scope}
			 \draw \circleA node[anchor=south east] {$A$};
			 \draw \circleB node[anchor=south west] {$B$};
		 \end{tikzpicture}
		 &
		 
		% A union B
		\begin{tikzpicture}
			\draw[fill=white] (0,0) -- (4,0) -- (4,3) -- (0,3) -- (0,0) node[anchor=south west] {U};
			\draw[fill=orange]  \circleA node[anchor=south east] {$A$}
								\circleB node[anchor=south west] {$B$};
		\end{tikzpicture}
		&
		
		% A - B
		 \begin{tikzpicture}
		     \draw[fill=white] (0,0) -- (4,0) -- (4,3) -- (0,3) -- (0,0) node[anchor=south west] {U};
		     \begin{scope}
		         \clip \circleA;{}
		         \draw[fill=orange, even odd rule]   \circleA node[anchor=south east] {$A$}
		                                             \circleB;
		     \end{scope}
		     \draw[]             \circleA node[anchor=south east] {$A$}
		                         \circleB node[anchor=south west] {$B$};
		 \end{tikzpicture}
		
		\\
		A diagram of $A \cap B$, &
		A diagram of $A \cup B$, &
		A diagram of $A - B$, \\
		$A$ intersection $B$. &
		$A$ union $B$. &
		The difference of $A$ and $B$. 
	\end{tabular}
}{}

\chapter{Questions}
\input{../BASE-3-INSTRUCTIONS-HOMEWORK}

	\stepcounter{question} \question{\thequestion}{Easy operations}{25\%}{}
        Given the sets:
        \begin{center}
            $A = \{ 1, 2, 3 \}$ \tab
            $B = \{ 2, 4, 6 \}$ \tab
            $C = \{ 6, 7 \}$
            ~\\ ~\\ and $U = \{ 1, 2, 3, 4, 5, 6, 7, 8, 9 \}$
        \end{center}
        Solve each of the following operations:
        
        \begin{figure}[h]
		    \centering
		    \begin{subfigure}{.5\textwidth}
		        \centering
					\begin{enumerate}
						\item[a.]   $A \cup B = $       \notonkey{ \vspace{0.7cm} }{ $\{ 1, 2, 3, 4, 6 \}$ }
						\item[b.]   $A \cap B = $       \notonkey{ \vspace{0.7cm} }{ $\{2 \}$ }
						\item[c.]   $A \cup C = $       \notonkey{ \vspace{0.7cm} }{ $\{ 1, 2, 3, 6, 7 \}$ }
						\item[d.]   $A \cap C = $       \notonkey{ \vspace{0.7cm} }{ $\{ \}$ or $\emptyset$ }
					\end{enumerate}
		           
		    \end{subfigure}%
		    \begin{subfigure}{.5\textwidth}
		        \centering
					\begin{enumerate}
						\item[e.]   $A - B = $          \notonkey{ \vspace{0.7cm} }{ $\{1, 3 \}$ }
						\item[f.]   $(A - B) \cup C = $ \notonkey{ \vspace{0.7cm} }{ $\{1, 3, 6, 7\}$ }
						\item[g.]   $A' = $ 			\notonkey{ \vspace{0.7cm} }{ $\{4, 5, 6, 7, 8, 9\}$ }
						\item[h.]   $U - A = $ 			\notonkey{ \vspace{0.7cm} }{ $\{4, 5, 6, 7, 8, 9\}$ }
					\end{enumerate}

		           
		    \end{subfigure}
		\end{figure}
	
	\newpage
	\stepcounter{question} \question{\thequestion}{Medium operations}{25\%}
		Given the sets:
        \begin{center}
            $A = \{ a, c, e \}$ \tab
            $B = \{ b, d, f \}$ \tab
            $C = \{ c, e, g \}$ \tab
            $D = \{ d, f, a \}$
            ~\\ ~\\ and $U = \{ a, b, c, d, e, f, g \}$
        \end{center}
        Solve each of the following operations:
        
        \begin{enumerate}
			\item[a.]	$A \cap C =$			\notonkey{ \vspace{0.5cm} }{ $\{ c, e \}$ }
			\item[b.]	$A \cup B =$			\notonkey{ \vspace{0.5cm} }{ $\{ a, b, c, d, e, f \}$ }
			\item[c.]	$(A \cup B) - C =$		\notonkey{ \vspace{0.5cm} }{ $\{ a, b, d, f \}$ }
			\item[d.]	$B - D = $				\notonkey{ \vspace{0.5cm} }{ $\{ b \}$ }
			\item[e.]	$A \cup (B - D) =$		\notonkey{ \vspace{0.5cm} }{ $\{ a, b, c, e \}$ }
			\item[f.]	$(A \cup B)' =$			\notonkey{ \vspace{0.5cm} }{ $\{ g \}$ }
			\item[g.]	$A' =$					\notonkey{ \vspace{0.5cm} }{ $\{ b, d, f, g \}$ }
			\item[h.]	$B' =$					\notonkey{ \vspace{0.5cm} }{ $\{ a, c, e, g \}$ }
			\item[i.]	$A' \cap B' =$			\notonkey{ \vspace{0.5cm} }{ $\{ g\}$ }
        \end{enumerate}
		
	\newpage
	\stepcounter{question} \question{\thequestion}{Set properties}{25\%}
	
		Identify whether each pair of operations will give the same results.
		If they do, label which property (Commutative, Associative, or Distributive) is
		being illustrated.
		
		\begin{center}
			Sets: $A = \{1, 2, 3\}$ \tab $B = \{ 3, 4, 5 \}$ \tab $C = \{2, 3\}$
		\end{center}
		
		\begin{tabular}{p{0.5cm} p{3cm} p{3cm} l}
					   & Operation 1			& Operation 2
			\\ \\	a. & $A \cap B$ 			& $B \cup A$ 					& \solution{not equivalent}{ \\ \\ }
			\\ \\   b. & $B \cup C$ 			& $C \cup B$ 					& \solution{equivalent; commutative property}{ \\ \\ }
			\\ \\   c. & $(A \cup B) \cup C$	& $A \cup (B \cup C)$			& \solution{equivalent; associative property}{ \\ \\ }
			\\ \\   d. & $(A \cup B) \cup C$	& $A \cap (B \cap C)$			& \solution{not equivalent}{ \\ \\ }
			\\ \\   e. & $A \cap (B \cup C)$	& $(A \cap B) \cup C$			& \solution{not equivalent}{ \\ \\ }
			\\ \\   f. & $A \cup (B \cap C)$	& $(A \cup B) \cap (A \cup C)$	& \solution{equivalent; distributive property}{ \\ \\ }
			\\ \\   g. & $A \cap (B \cup C)$	& $(A \cup B) \cap (A \cup C)$	& \solution{not equivalent}{ \\ \\ }
		\end{tabular}
    
    \newpage
    \stepcounter{question} \question{\thequestion}{Venn diagrams}{25\%}
    
        Fill in the Venn diagrams for the following operations:

		\def\circleA{(1.5,2.5) circle (1.0cm)}
		\def\circleB{(2.5,2.5) circle (1.0cm)}
		\def\circleC{(2.0,1.5) circle (1.0cm)}
		
        \begin{center}
            \begin{tabular}{l l l}
                a.      $A \cup B$
                & b.    $A \cup C$
                & c.    $A \cup B \cup C$
                \\
                % A \cup B
                \solution{
					%\begin{tikzpicture}
						%\draw[fill=white] (0,0) -- (4,0) -- (4,3) -- (0,3) -- (0,0) node[anchor=south west] {U};
						%\draw[fill=white]	\circleC node[anchor=south] {$C$};
						%\draw[fill=pink]  	\circleA node[anchor=south east] {$A$}
											%\circleB node[anchor=south west] {$B$};
					%\end{tikzpicture}
					\begin{tikzpicture}
						\draw (0,0) -- (4,0) -- (4,4) -- (0,4) -- (0,0) node[anchor=south west] {U};
						\draw[fill=pink] 	\circleA circle (1cm) node[anchor=south east] {A}
											\circleB circle (1cm) node[anchor=south west] {B};
						\draw \circleC circle (1cm) node[below] {C};
					\end{tikzpicture}
                }{\venndiagram}
                &
                % A \cup C
                \solution{
					\begin{tikzpicture}
						\draw (0,0) -- (4,0) -- (4,4) -- (0,4) -- (0,0) node[anchor=south west] {U};
						\draw[fill=pink] 	\circleA circle (1cm) node[anchor=south east] {A}
											\circleC circle (1cm) node[below] {C};
						\draw \circleB circle (1cm) node[anchor=south west] {B};
					\end{tikzpicture}
                }{\venndiagram}
                &
                % A \cup B \cup C
                \solution{
					\begin{tikzpicture}
						\draw (0,0) -- (4,0) -- (4,4) -- (0,4) -- (0,0) node[anchor=south west] {U};
						\draw[fill=pink] 	\circleA circle (1cm) node[anchor=south east] {A}
											\circleB circle (1cm) node[anchor=south west] {B}
											\circleC circle (1cm) node[below] {C};
					\end{tikzpicture}
                }{\venndiagram}
                \\ \\
                d. $A'$
                & e. $A \cap B$
                & f. $(A \cap B)'$
                \\
                % A'
                \solution{
					\begin{tikzpicture}
						\draw[fill=pink] (0,0) -- (4,0) -- (4,4) -- (0,4) -- (0,0) node[anchor=south west] {U};
						\draw[fill=white] \circleA circle (1cm) node[anchor=south east] {A};
						\draw \circleB circle (1cm) node[anchor=south west] {B};
						\draw \circleC circle (1cm) node[below] {C};
					\end{tikzpicture}
                }{\venndiagram}
                &
                % A \cap B
                \solution{
					\begin{tikzpicture}
						\draw (0,0) -- (4,0) -- (4,4) -- (0,4) -- (0,0) node[anchor=south west] {U};
						 \begin{scope}
							 \clip \circleA;
							 \fill[fill=pink] \circleB;
						 \end{scope}
						\draw \circleA circle (1cm) node[anchor=south east] {A};
						\draw \circleB circle (1cm) node[anchor=south west] {B};
						\draw \circleC circle (1cm) node[below] {C};
					\end{tikzpicture}
                }{\venndiagram}
                &
                % (A \cap B)'
                \solution{
					\begin{tikzpicture}
						\draw[fill=pink] (0,0) -- (4,0) -- (4,4) -- (0,4) -- (0,0) node[anchor=south west] {U};
						 \begin{scope}
							 \clip \circleA;
							 \fill[fill=white] \circleB;
						 \end{scope}
						\draw \circleA circle (1cm) node[anchor=south east] {A};
						\draw \circleB circle (1cm) node[anchor=south west] {B};
						\draw \circleC circle (1cm) node[below] {C};
					\end{tikzpicture}
                }{\venndiagram}
                \\ \\
                g.      $(A \cup B) - C$
                & h.    $(A \cap B) \cup C$
                & i.    $A - (B \cap C)$
                \\
                % (A \cup B) - C
                \solution{
					\begin{tikzpicture}
						\draw (0,0) -- (4,0) -- (4,4) -- (0,4) -- (0,0) node[anchor=south west] {U};
						 \begin{scope}
							 \clip \circleA;{}
							 \draw[fill=pink, even odd rule]   	\circleA node[anchor=south east] {}
																\circleC;
						 \end{scope}
						 \begin{scope}
							 \clip \circleB;{}
							 \draw[fill=pink, even odd rule]   	\circleB node[anchor=south east] {}
																\circleC;
						 \end{scope}
						\draw	\circleA circle (1cm) node[anchor=south east] {A};
						\draw	\circleB circle (1cm) node[anchor=south west] {B};
						\draw 	\circleC circle (1cm) node[below] {C};
					\end{tikzpicture}
                }{\venndiagram}
                &
                % (A \cap B) \cup C
                \solution{
					\begin{tikzpicture}
						\draw (0,0) -- (4,0) -- (4,4) -- (0,4) -- (0,0) node[anchor=south west] {U};
						 \begin{scope}
							 \clip \circleA;
							 \fill[fill=pink] \circleB;
						 \end{scope}
						\draw \circleA circle (1cm) node[anchor=south east] {};
						\draw \circleB circle (1cm) node[anchor=south west] {};
						\draw[fill=pink] \circleC circle (1cm) node[below] {};
						
						\draw \circleA circle (1cm) node[anchor=south east] {A};
						\draw \circleB circle (1cm) node[anchor=south west] {B};
						\draw \circleC circle (1cm) node[below] {C};
					\end{tikzpicture}
                }{\venndiagram}
                &
                % A - (B \cap C)
                \solution{
					\begin{tikzpicture}
						\draw (0,0) -- (4,0) -- (4,4) -- (0,4) -- (0,0) node[anchor=south west] {U};
						\draw[fill=pink] \circleA circle (1cm) node[anchor=south east] {};
						 \begin{scope}
							 \clip \circleB;
							 \fill[fill=white] \circleC;
						 \end{scope}
						\draw \circleB circle (1cm) node[anchor=south west] {};
						\draw \circleC circle (1cm) node[below] {};
						\draw \circleA circle (1cm) node[anchor=south east] {A};
						\draw \circleB circle (1cm) node[anchor=south west] {B};
						\draw \circleC circle (1cm) node[below] {C};
					\end{tikzpicture}
                }{\venndiagram}
            \end{tabular}
        \end{center}
    
\input{../BASE-4-FOOT}
