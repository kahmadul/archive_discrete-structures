def Search( myList, n, findMe ):
    for i in range( 0, n ):
        if myList[i] == findMe:
            return i
    return -1

def IsInList( myList, item ):
    for el in myList:
        if el == item:
            return True
    return False



def TimesTables( n ):
    for y in range( n ):
        for x in range( n ):
            print( x, y, x*y )



TimesTables( 5 )
