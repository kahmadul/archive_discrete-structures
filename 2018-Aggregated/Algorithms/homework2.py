#INPUTS:    searchList: A list of values to search through
#           findMe: The value to find in the list

#OUTPUTS:   -1: Returned when the findMe item is not found
#           index: The index where findMe is found in the list

def LinearSearch( searchList, findMe ):

    # Search every item in the list
    for i in range( 0, len( searchList ) ):
        if ( searchList[i] == findMe ):
            return i    # Found at this index
            
    return -1 # Not found
